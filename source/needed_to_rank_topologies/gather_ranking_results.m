% © 2021, ETH Zurich
function RankedCircuits = gather_ranking_results( run)

% create a structure that summarizes important features of functional
% circuits.
% this code a bit ugly enables to run the function with a parfor if we want

    foldername = ['output_files/ranked_topologies/' run '/'];
    filenames = dir([foldername 'OutV*']);
    nFiles = size(filenames,1);
    
    RankedCircuits_OutV_id = cell(nFiles,1); 
    RankedCircuits_circuitnames = cell(nFiles,1); 
    RankedCircuits_complexity = cell(nFiles,1);        
    RankedCircuits_logrobustness = cell(nFiles,1);            
    RankedCircuits_logrobustness_err = cell(nFiles,1);            
    RankedCircuits_ideal_feasibility = cell(nFiles,1);    
    RankedCircuits_updated_feasibility = cell(nFiles,1);             
    RankedCircuits_IFF = cell(nFiles,1);             
    RankedCircuits_PF = cell(nFiles,1);   
    RankedCircuits_NF = cell(nFiles,1);   
    
    functional = zeros(nFiles,1);
        
    for iFile= 1:nFiles 
 
            
        OutVfilename = filenames(iFile).name;
        loaded = load([foldername OutVfilename]);
        OutV = loaded.OutV;    
    
        if isfield(OutV, 'vol') && (OutV.robustness > 0)
            % means that the circuit is functional
            
            
            RankedCircuits_OutV_id{iFile} = OutV.id;
            fprintf('gather circuit %d \n', OutV.id);
            RankedCircuits_circuitnames{iFile} = OutV.circuitnames;
            RankedCircuits_complexity{iFile} = numel(OutV.circuitnames);  
            RankedCircuits_IFF{iFile} = OutV.IFF;
            RankedCircuits_PF{iFile} = OutV.PF;
            RankedCircuits_NF{iFile} = OutV.NF;
            functional(iFile) = 1;
            RankedCircuits_logrobustness{iFile} = log10(OutV.robustness);                                       
            RankedCircuits_logrobustness_err{iFile} = OutV.robustness_err./OutV.robustness; % error propagation                        
        

            if isfield(OutV, 'ideal_feasibility')
                RankedCircuits_ideal_feasibility{iFile} = OutV.ideal_feasibility; 
            else 
                RankedCircuits_ideal_feasibility{iFile} = -1;
            end


            if isfield(OutV, 'updated_feasibility')                
                RankedCircuits_updated_feasibility{iFile} = OutV.updated_feasibility;  
                else 
                RankedCircuits_updated_feasibility{iFile} = -1;
            end

        end
        

    end
    RankedCircuits = struct(...
        'OutV_id',RankedCircuits_OutV_id, ...
        'circuitnames',RankedCircuits_circuitnames, ...
        'complexity', RankedCircuits_complexity, ...        
        'IFF',RankedCircuits_IFF,...
        'PF', RankedCircuits_PF,...
        'NF',RankedCircuits_NF,... 
        'logrobustness',RankedCircuits_logrobustness,...
        'logrobustness_err',RankedCircuits_logrobustness_err,...                     
        'ideal_feasibility', RankedCircuits_ideal_feasibility, ...
        'updated_feasibility', RankedCircuits_updated_feasibility    );
    
    RankedCircuits = RankedCircuits(find(functional));
    
    [~,index] = sortrows([RankedCircuits.OutV_id].');
    RankedCircuits = RankedCircuits(index);
    for k = 1:length(index)
        RankedCircuits(k).publication_id = k;
    end
    
    

    save(['output_files/ranked_topologies/' run '/RankedCircuits.mat'],'RankedCircuits')


end