% © 2021, ETH Zurich
function [] = plot_figure_5C()

    loaded = load('output_files/data/dataPulsealpha_durations.mat');
    dataPulsealpha = loaded.dataPulsealpha;
    strains = dataPulsealpha.strains;
    percent_higher = calc_percent_higher(dataPulsealpha);
    
    
    close all;
    
    figure()
    clf;
    strainsubset = {'FRY2909' 'FRY2941' 'FRY2952' 'FRY2957' 'FRY2958' 'FRY2959' 'FRY2960'};    
    strain_names = {'C_2' 'C_1' 'C_3' 'T_{39.1}' 'T_{39.2}' 'T_{39.3}' 'T_{39.4}'};
    idx = find(ismember(strains,strainsubset));
    h = plot(dataPulsealpha.alphapulse, squeeze(percent_higher(1,idx,:)),'o','LineWidth',1,'MarkerSize',8);
    set(h, {'color'},{'k'; 'k'; 'k'; 'r'; 'r'; 'r'; 'r'});
    hold on;
    h = plot(dataPulsealpha.alphapulse, squeeze(percent_higher(1,idx,:)),'-','LineWidth',1);
    set(h, {'color'},{'k'; 'k'; 'k'; 'r'; 'r'; 'r'; 'r'});
    xlim([0 180]);    
    legend(strain_names,'Location','northwest');
    xlabel('pulse duration')
    ylabel('cells ON at 6h (%)');
    set(gca,'TickLength',[0 0]);
    set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 0.8 1]);

    print('plots/figure_5C', '-dpdf'); 

    
end