% © 2021, ETH Zurich
function [] = run_decoder_motifs()
%% Simplified decoder models based on FFLs / NF

%% parameters and constants
odefun = @ode_decoder;

% kinetic parameters (default)
By  = 0;
bey = 1;
Kxy = 0.1;
dy  = 0.5;
Bz  = 0;
bez = 1;
Kxz = 0.1;
Kyz = 0.01;
dz  = 0.1;
H   = 2;
Bo  = 0;
beo = 1;
Kzo = 0.1;
Kyo = 0.1;
do  = 1;

% kinetic parameters (negative feedback)
KyzNF = 0.001;
KzyNF = 0.01;
dyNF  = 0.125;
dzNF  = 0.2;

% compile parameter vector
para = [By; bey; Kxy; dy; Bz; bez; Kxz; Kyz; dz; ...
    H; Bo; beo; Kzo; Kyo; do; KyzNF; KzyNF; dyNF; dzNF];

% simulation parameters
npulse = 1; % pulses
tpulse = 15; % time between pulses
dpulse = [0 1 2 5 10 20 30]; % pulse duration
dpplot = 2; % pulse duration to plot
tmax   = 60; % simulation time
scf    = 1e3; % scaling factor

% state indices
idxiff   = 1:2; % IFF
idxcff   = 3:4; % CFF
idxecff  = 5:7; % 4-node CFF
idxdec   = 8:10; % IFF-based decoder
idxnf    = 11:12; % NF
idxdecnf = 13:15; % NF-based decoder

idxstate = {idxiff,idxcff,idxecff,idxdec,idxnf,idxdecnf};
nx = max(cellfun(@max,idxstate))+1;

%% run simulation

figure(2);clf

for zd = 1:length(dpulse) % loop: pulse duration
    
    disp(dpulse(zd));
    
    [~]    = odefun([],[],para,0,idxstate);
    [~,y0]  = ode15s(odefun,[0 10*tpulse],zeros(nx,1),idxstate);
    
    ut0 = [0 tpulse tpulse tpulse+dpulse(zd) tpulse+dpulse(zd) tmax];
    ux0 = [0 0 1 1 0 0];
    
    for zp = 1:npulse % loop: pulse trains
        [t1,y1] = pulse_decoder(odefun,tpulse,dpulse(zd),tmax,para,y0,idxstate);
        
        if zp ==1
            t = t1;
            yc = y1;
            ut = ut0;
            ux = ux0;
        else
            toff = t(end);
            t = [t; t1+toff];
            yc = [yc; y1];
            ut = [ut, ut0+toff];
            ux = [ux, ux0];
        end
        y0 = y1;
    end
    
    y = yc ./ (ones(size(yc,1),1)*max(yc)); % normalized by max
    
    if dpulse(zd) == dpplot
        
        figure(1);
        clf
        
        subplot(3,2,1)
        stairs(ut,ux,'k-','LineWidth',1); hold on;
        plot(t,y(:,idxiff),'LineWidth',1);
        title('3-node IFF');
        legend('X','Y','Z','Location','best');
        axis([0 tmax -.05 1.05]);
        ylabel('Concentration (-)');
        
        subplot(3,2,2)
        stairs(ut,ux,'k-','LineWidth',1); hold on;
        plot(t,y(:,idxcff),'LineWidth',1);
        title('3-node CFF');
        legend('Y','Z','O','Location','best');
        axis([0 tmax -.05 1.05]);
        
        subplot(3,2,3)
        stairs(ut,ux,'k-','LineWidth',1); hold on;
        plot(t,y(:,idxecff),'LineWidth',1);
        title('4-node CFF');
        legend('X','Y','Z','O','Location','best');
        axis([0 tmax -.05 1.05]);
        ylabel('Concentration (-)');
        
        subplot(3,2,4)
        stairs(ut,ux,'k-','LineWidth',1); hold on;
        plot(t,y(:,idxdec),'LineWidth',1);
        title('IFF-based decoder');
        legend('X','Y','Z','O','Location','best');
        axis([0 tmax -.05 1.05]);
        
        subplot(3,2,5)
        stairs(ut,ux,'k-','LineWidth',1); hold on;
        plot(t,y(:,idxnf),'LineWidth',1);
        title('NF');
        legend('X','Y','Z','Location','best');
        axis([0 tmax -.05 1.05]);
        ylabel('Concentration (-)');
        xlabel('Time (-)')
        
        subplot(3,2,6)
        stairs(ut,ux,'k-','LineWidth',1); hold on;
        plot(t,y(:,idxdecnf),'LineWidth',1);
        title('NF-based decoder');
        legend('X','Y','Z','O','Location','best');
        axis([0 tmax -.05 1.05]);
        xlabel('Time (-)')
        
        set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
        print('plots/figure_S3B', '-dpdf'); 
    end
    
    figure(2)
    subplot(2,2,1)
    plot(t,scf*yc(:,idxdec(end)),'LineWidth',1);
    hold on
    title('IFF-based decoder');
    ylabel('Scaled output O (-)')
    xlabel('Time (-)')
    
    subplot(2,2,2)
    plot(t,scf*yc(:,idxdecnf(end)),'LineWidth',1);
    hold on
    title('NF-based decoder');
    xlabel('Time (-)')
    
end

figure(2)
subplot(2,2,1)
legend(num2str(dpulse'),'Location','best')
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
print('plots/figure_S3C', '-dpdf'); 

return