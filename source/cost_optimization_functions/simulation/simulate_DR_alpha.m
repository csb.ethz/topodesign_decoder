% © 2021, ETH Zurich
function SData  = simulate_DR_alpha(dataDRalpha, SData, expname, P, modelname, ICRef, PARAName, FlPlot, icase, modulename, minstd)

            % simulate response to alpha factor constant doses and update
            % SData with simulated response (FP) and experimental data
            % (means and std)
            
                
            % prepare simulated data structure
            dose = dataDRalpha.alphadose;
            idx_0 = (dose == 0);
            dose(idx_0) = 10^(-0.7); % small number to avoid problems with log
            SData.(expname).dose = dose;          
            timepoints = dataDRalpha.timepoints;
            SData.(expname).timepoints = timepoints;
            ntimepoints = length(dataDRalpha.timepoints);  
            ndose = length(dose);  
            
            SData.(expname).FP =  zeros(ntimepoints,ndose);
            SData.(expname).std = zeros(ntimepoints,ndose);
            SData.(expname).FRYname = modulename;
                   
            % calculate scaling factor based on the data to map apu and
            % fluorescence
			idx_alpha0 = find(dataDRalpha.alphadose == 0);
            idx_time6h = find(dataDRalpha.timepoints == 360);			
            meancontrol0 = dataDRalpha.FRY2932.means(1,1); % not impacted by alpha
			stdcontrol0 = dataDRalpha.FRY2932.std(1,1);
            meancontrolAct1 = dataDRalpha.FRY2908.means(idx_time6h,idx_alpha0);			
            SData.(expname).x_scal = P.rho*(P.k_maturation + P.rho)/P.k_maturation*(meancontrolAct1 - meancontrol0); % scaling factor (Fact - F0)*rho*(km+rho)/km        
                 
                       
            
            % induce at t = induction_time with estradiol, aTc and alpha factor
            % dose
            
            P.est = 5000;  
            P.ind_time = 720; % 12h
            P.ind_duration = timepoints(end); % alpha factor step function 
            exp_tspan = 0:1:timepoints(end);
            tspan = [0:10:(P.ind_time - 4) (P.ind_time - 3):1:(P.ind_time - 1) P.ind_time + exp_tspan]; 
            % try to use as few data points as possible in tspan to make it faster
            
            % initialize simulated results matrix 
            FPContinuousOutput = zeros(length(tspan),ndose);
            LexAContinuousOutput = zeros(length(tspan),ndose);
                
            for kdose = 1:ndose
                
                P.alphamax = dose(kdose);                
                para = fconvpara(P);   
                output = IQMPsimulate(modelname, tspan , ICRef, PARAName, para);                                                
                indexFP = ismember(output.states, 'FP');  
                indexLexA = ismember(output.variables, 'TF1on'); 
                idxtime = find(ismember(tspan - P.ind_time,timepoints));
                idxtimeEnd = find(ismember(tspan - P.ind_time,timepoints(end)));
                
                
                % report simulated data and normalized experimental data 
                FPContinuousOutput(:,kdose) = SData.(expname).x_scal*output.statevalues(:,indexFP);
                SData.(expname).FP(:,kdose) = meancontrol0 + FPContinuousOutput(idxtime,kdose);
                LexAContinuousOutput(:,kdose) = SData.(expname).x_scal*output.variablevalues(:,indexLexA);
                SData.(expname).LexA(:,kdose) = LexAContinuousOutput(idxtime,kdose);
                SData.(expname).means(:,kdose) = dataDRalpha.(modulename).means(1:ntimepoints,kdose);      
                              
                % error propagation when substracting control
                SData.(expname).std(:,kdose) = max([minstd*ones(ntimepoints,1); sqrt(dataDRalpha.(modulename).std(1:ntimepoints,kdose) + stdcontrol0).^2]);   
  
            end              
            
            % plot simulation vs data
            if FlPlot     
                figure(icase + P.tc)
                clf;
                
                % plot kinetics
                subplot(2,1,2);
                map = parula(ndose + 2);
                map = flipud(map(1:ndose,:));
                h = plot((tspan(idxtime(1):end) - P.ind_time)./60,FPContinuousOutput(idxtime(1):end,:));
                set(h, {'color'}, num2cell(map,2));
                colormap(map);

                hold on;

                for kdose = 1:ndose                    
                    errorbar(timepoints./60,SData.(expname).means(1:ntimepoints,kdose),SData.(expname).std(1:ntimepoints,kdose),'o','LineWidth',0.75,'color',map(kdose,:),'CapSize',0,'MarkerSize',15);
                    hold on;
                end
                xlabel('time after induction (h)')
                ylabel(['Fluorescence of ' modulename]) 
                yLim = get(gca,'YLim');
                set(gca,'YLim', [0 yLim(2)],'TickLength',[0 0]);
                                    
                % plot dose response
                subplot(2,1,1)
                title(expname,'Interpreter','none')
                    
                hold on;
                for kdose = 1:ndose                    
                    errorbar(log10(dose(kdose)),SData.(expname).means(end,kdose),SData.(expname).std(end,kdose),'o','LineWidth',0.75,'color',map(kdose,:),'CapSize',0,'MarkerSize',15);
                    hold on;
%                         plot(log10(dose(kdose)),FPContinuousOutput(idxtimeEnd,kdose),'.','MarkerSize',10,'color',map(kdose,:));
                end
                plot(log10(dose),FPContinuousOutput(idxtimeEnd,:),'-','color',[0.3 0.3 0.3],'LineWidth',0.75);
                xlabel('log alpha (nM)')
                xlim([-0.7 3])
                ylabel('Fluorescence at 6h')  
                yLim = get(gca,'YLim');
                set(gca,'YLim', [0 yLim(2)],'TickLength',[0 0]);                          
            end
            
end            