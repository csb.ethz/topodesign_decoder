% © 2021, ETH Zurich
function [] = plot_figure_S8()
loaded = load('output_files\ranked_topologies\190927_172938\OutV_78.mat');
OutV = loaded.OutV;

nfactors = length(OutV.updated_tuning_factors);
bmin = ones(nfactors,1);
islog = ones(nfactors,1);
bmax = 10.^max(max(OutV.updated_feasibility_region.V))*ones(nfactors,1);
tuning_index_in_circuitSubspace = find_tuning_idx_copynumbers_tc(OutV.circuitnamesInSubspace);
names = OutV.circuitnamesInSubspace(tuning_index_in_circuitSubspace);
paramSpecs = table(names, bmin, bmax,islog);
figure();
plot_OutV(OutV.updated_feasibility_region, paramSpecs)
set(gcf,'PaperOrientation','portrait','PaperUnits','Normalized','PaperPosition',[0 0 1 0.7]);
print('plots/figure_S8', '-dpdf'); 

end