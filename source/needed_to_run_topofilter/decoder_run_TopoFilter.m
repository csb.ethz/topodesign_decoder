% © 2021, ETH Zurich
function [ exit_status, results_fn, elapsed_time ] = decoder_run_TopoFilter( varargin )

    % this function runs TopoFilter to find topologies that could be
    % functional
    
    assert(exist('TFmain','file') == 2,...
        'Couldn''t find `TFmain` function. Be sure to add TopoFilter source code folder to the path.');

    timestamp = @() datestr(now, 'yyyy.mm.dd HH:MM');

    %% Initialise
	rng('shuffle');

    fprintf('Program started at %s\n',timestamp());
	tic;


    %% Tunable experiments setting if not provided via function options (+ validation)
    parser = inputParser;
    parser.FunctionName = 'TEMPLATEEXPERIMENT: ';

    % Results file    
    start = datestr(now, 'yymmdd_HHMMSS');
    addParameter(parser, 'outputFilename', fullfile('output_files/TopoFilter_output_samples/', start,...           
       sprintf('%s.mat',datestr(now, 'yymmdd_HHMMSS'))), @ischar);

    % Number of runs
    addParameter(parser, 'numRuns', 1, @isposint);
    % Dry run: calc only threshold (and p0, if not given)
    addParameter(parser, 'dryRun', false, @islogical);

    % Sample sizes: not directly options itself, but used below for setting
    % the actual nmont, nelip and nvmin options.
    % Maximum number of cost function evauations in one sampling (roughly).
    addParameter(parser, 'nfeval', 5e4, @isposint);
    % Minimal size of viable points sample
    addParameter(parser, 'nvmin', 2e4, @isposint);
    % Maximal size of viable points sample
    addParameter(parser, 'nvmax', 1e5, @isposint);

    % Flag for saving viable points after each sampling
    addParameter(parser, 'saveViablePoints', true, @islogical);
    % Flag for removing the MEX files after the runs
    % Set to `false` if you're independently running `TFmain` using the same
    % model file (e.g. when aditionally externally parallelising experiments)
    addParameter(parser, 'cleanupMexFiles', true, @islogical);

    % Flag indicating if viable points found with the adaptive MCMC method
    % should be dropped.
    addParameter(parser, 'dropMontPoints', false, @islogical);

    % Flags to control flow of the model space search
    % * Recursive search
    addParameter(parser, 'recursive', true, @islogical);
    % * Enumeration level for the viable projections that allows to switch the
    %   heuristic between goals of: a) finding a maximal reduction (0), and
    %   b) finding as many reductions as possible (2)
    addParameter(parser, 'enumLevel', 1, isinrange(2));
    % * Number of (couples of) parameters to combine at most at once during the
    %   initial exhaustive search for the viable projections (`1` = singletons,
    %   `2` = singletons and pairs etc.)
    addParameter(parser, 'exhaustiveRank', 1, @isposint);
    % EXPERIMENTAL, do not use unless you know what you're doing
    addParameter(parser, 'backtrack', false, @islogical);
    addParameter(parser, 'pointsAsOuter', true, @islogical);

    % Parallel computation level
    % 0 - none
    % 1 - over the OAT viability checks in getViableProjections
    % 2 - over projections (in the recursive search) and OAT checks (pre-recursion)
    % 3 - over runs
    
    % parallelize only on the cluster
    if any(ismember(parallel.clusterProfiles,'SGE_BSSE'))
        addParameter(parser, 'parallelize', 1, isinrange(3));
    else
        addParameter(parser, 'parallelize', 0, isinrange(3));
    end

    % Param space exploration specification
    addParameter(parser, 'paramSpecsFilename', 'source/models/paramSpecs.txt', @ischar);
    addParameter(parser, 'paramCouplingFcn', @decoder_couplingMatrix, @isf);

    % Init param value
    % p0 obtained previoulsy
    loaded = load('source/needed_to_run_topofilter/initialpointslist_decoder.mat');
    p0 = loaded.paraoptlist; 
    addParameter(parser, 'p0', p0, @isnumeric);

    % Cost function and extra arguments
    addParameter(parser, 'calcCostFcn', @decoder_calcCost, @isf);
    % Extract data function and extra arguments
    addParameter(parser, 'extractSimDataFcn', @decoder_extractSimData, @isf);    
    % Viability threshold function and extra (non-default) arguments
    addParameter(parser, 'calcThresholdFcn', @decoder_calcThreshold, @isf);

    % ODE integrator options
    % options structure with as specified in `IQMPsimulate`
    simopts.reltol = 1e-6;
    simopts.abstol = 1e-6;
    simopts.maxnumsteps = 1e6;
    addParameter(parser, 'odeIntegratorSolNrTimepoints', 820, @isint);  % 400 min to reach SS + 420 min simulation  
    addParameter(parser, 'odeIntegratorOptions', simopts, @isstruct);
    % number of integration attempts in case of an error; each next with
    % 10-times previous `maxnumsteps` (default: `1e5`)
    addParameter(parser, 'odeIntegratorMaxnumstepsTry', 1, @isposint);
    
    % Parse the input arguments
    parse(parser, varargin{:});
    p = parser.Results;

    %% start parallel pool from the beginning    
    if p.parallelize == 1        
        n = 100;
        startPoolCluster(n);
    end
    %% Model, experiment and data files
    % SBML (TXT/XML) or IQMmodel (TXT/Matlab) format
    p.modelFilename = 'source/models/decoder_model.txtbc';
    % IQMexperiment format; string or cell array of string;
    % one experiment expected per each data file
    p.expFilename = {'source/needed_to_run_topofilter/exp-none.exp','source/needed_to_run_topofilter/exp-short20.exp', 'source/needed_to_run_topofilter/exp-short30.exp','source/needed_to_run_topofilter/exp-long.exp'};
%   % IQMmeasurement format (CSV or XLS); string or cell arrray of string;
    % in case of XLS each sheet (except for first) is treated like a single CSV
    p.expDataFilename = 'source/needed_to_run_topofilter/expData.xls';    
    % Indices for data files to use (helpful w/ mult-sheet XLS); [] = all
    p.expDataIdxs = 1:4;

    %% Sample sizes
    % Split nfeval according to given proportions
    p.rfeval = [1 1];
    nfevalv = splitlrm(p.nfeval,  p.rfeval/sum(p.rfeval));
    % Maximum target sample size in the Markov chain Monte Carlo (MCMC) sampling
    p.nmont = nfevalv(1);
    % Guiding number of model evaluations in the Ellipsoids-based sampling
    p.nelip = nfevalv(2);


	%% Run the actual program
    [ runs, viabilityThreshold, settings ] = TFmain(p);

	%% Save results
    results_fn = settings.outputFilename;
    settings.nfeval = p.nfeval;
    save(results_fn, 'runs', 'viabilityThreshold', 'settings');

	%% Cleanup
    elapsed_time = toc;
    fprintf('Program finished at %s\n',timestamp());

    exit_status = 0;
end
