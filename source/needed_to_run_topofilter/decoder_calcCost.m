% © 2021, ETH Zurich
function cost  = decoder_calcCost(simDataPts, varargin )

    % simDataPts contains
    % * in rows experiments (none, short20, short30, long), and
    % * in columns time points (max value, t0, t0+6h)
    max_none = simDataPts(1, 1); none_t0 = simDataPts(1, 2);
    short20_t6h = simDataPts(2, 3);
    short30_t6h = simDataPts(3, 3);
    max_long = simDataPts(4, 1);

    basal = none_t0/100;
    response = max(100/short20_t6h,100/short30_t6h);
    sensitivity = 20*max_none/short30_t6h;
    noresponsetolong = 20*max_long/short30_t6h;
    cost = max([basal^2 sensitivity^2 noresponsetolong^2 response^2]);

    % disp(table(basal,sensitivity,noresponsetolong,response))

end
