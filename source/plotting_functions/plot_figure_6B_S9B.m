% © 2021, ETH Zurich
function [] = plot_figure_6B_S9B

%% load copy numbers obtained by qPCR on different days
loaded = load('output_files\data\copy_numbers_1-4.mat');
copy_numbers = loaded.copy_numbers;
tuning_factors_LexA = copy_numbers.LexA;
std_LexA = copy_numbers.stdLexA;
tuning_factors_TetR = copy_numbers.TetR;
std_TetR = copy_numbers.stdTetR;
tuning_factors_TetR_triplicates = copy_numbers.TetR_triplicates;

loaded = load('output_files\data\copy_numbers_5-7.mat');
copy_numbers = loaded.copy_numbers;
tuning_factors_LexA = [tuning_factors_LexA;  copy_numbers.LexA];
std_LexA = [std_LexA;  copy_numbers.stdLexA];
tuning_factors_TetR = [tuning_factors_TetR;  copy_numbers.TetR];
std_TetR = [std_TetR;  copy_numbers.stdTetR];
tuning_factors_TetR_triplicates = [tuning_factors_TetR_triplicates;  copy_numbers.TetR_triplicates];

% aTcopt for circuit variants 1-4 obtained with figure S9B.
aTcopt = log10([137 196 47 67]'); 

strainstoplot = {'1' '2' '3' '4' '5' '6' '7' 'opt'};

%% regression to obtain remaining aTcopt (figure S9C)

Y = [aTcopt;aTcopt;aTcopt] ;
X = [ones(12,1) [tuning_factors_TetR_triplicates(1:4,1); tuning_factors_TetR_triplicates(1:4,2); tuning_factors_TetR_triplicates(1:4,3)]];

% data is only from 1:21. 22:24 is concentration 0
[b1,bint,r,rint,stats] = regress(Y, X);
figure();
clf;
plot([0 2],b1(2,:).*[0 2]+b1(1,:),'r-');
hold on;
p = plot(tuning_factors_TetR_triplicates(1:4,:),aTcopt,'ro');
set(p,'MarkerSize',15);
p = errorbarxy(tuning_factors_TetR(5:7),b1(2,:).*tuning_factors_TetR(5:7)+b1(1,:),std_TetR(5:7),[],{'ko', 'k', 'k'});
set(p.hMain,'MarkerSize',15);
xlabel('N_{fus1tet-LexA}')
ylabel('aTc^{opt}')

axis([0 2 0 3])
circuits_id = [5:7]';
predicted_aTc = b1(2,:).*tuning_factors_TetR_triplicates(5:7,:)+b1(1,:);
disp(table(circuits_id,10.^predicted_aTc ));
r2 = stats(1);
fprintf('the correlation coefficient r is %f \n',sqrt(r2));


aTcopt = [aTcopt; mean(predicted_aTc')'];
set(gca,'FontSize',20);
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 0.8 1]);

print('plots/figure_S9B', '-dpdf'); 

%%
loaded = load('output_files\ranked_topologies\190927_172938\OutV_78.mat');
region = loaded.OutV.updated_feasibility_region_wo_k12;
tuning_factors_opt = loaded.OutV.updated_tuning_factors_wo_k12;
tuning_factors_LexA = [tuning_factors_LexA; tuning_factors_opt(1)];
tuning_factors_TetR = [tuning_factors_TetR; tuning_factors_opt(2)];
std_LexA = [std_LexA; 0];
std_TetR = [std_TetR; 0];
aTcopt = [aTcopt; tuning_factors_opt(3)];

idx_list = [ 1 2; 2 3; 1 3];

figure(1);
clf;
for k= 1:3
    
subplot(2,2,k+1)
[~,density,X,Y] = kde2d([region.V(:,idx_list(k,1)),region.V(:,idx_list(k,2))],100);
contour(X,Y,density,[0.5 0.5],'LineWidth',1,'Color','r');
hold on;

end


subplot(2,2,2)
axis([0 2 0 2])
p = errorbarxy(tuning_factors_LexA, tuning_factors_TetR, std_LexA,  std_TetR, {'ko', 'k', 'k'} );
text(tuning_factors_LexA,tuning_factors_TetR,strainstoplot)
xlabel('N_{fus1tet-LexA}')
ylabel('N_{fus1-TetR}')
set(p.hMain,'MarkerSize',10);
set(gca,'TickLength',[0 0]);

subplot(2,2,3)
p = errorbarxy(tuning_factors_TetR, aTcopt, std_TetR, [],{'ko', 'k', 'k'} );
text(tuning_factors_TetR,aTcopt,strainstoplot)
set(gca,'TickLength',[0 0]);
set(p.hMain,'MarkerSize',10);
xlabel('N_{fus1-TetR}')
ylabel('aTc^{opt}')
axis([0 2 0 3])

subplot(2,2,4)
p = errorbarxy(tuning_factors_LexA, aTcopt, std_LexA, [],  {'ko', 'k', 'k'} );
text(tuning_factors_LexA,aTcopt,strainstoplot)
xlabel('N_{fus1tet-LexA}')
ylabel('aTc^{opt}')
set(p.hMain,'MarkerSize',10);
set(gca,'TickLength',[0 0]);
axis([0 2 0 3])

set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 0.8 1]);
print('plots/figure_6B', '-dpdf'); 

end