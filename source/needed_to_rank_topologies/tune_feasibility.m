% © 2021, ETH Zurich
function [max_feasibility, tuning_factors,  idx_viable] = tune_feasibility(OutV, posterior, find_index_function, paramSpecs, mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames)

    % maximizes the feasibility to find either ideal or updated
    % feasibility.
    
    % tuning factors provides indices of parameters to tune and tuning setup 
    % (parameters allowed to increase, decrease or both)
    
    % posterior = parameter posterior that can be the ideal gaussian or the
    % approximate bayesian posterior estimated with data
    % it has to be mapped to the circuit same Km subspace
    
    % OutV = information on the topology including uniformly distributed
    % viable points
    
    circuitnamesInSubspace = OutV.circuitnamesInSubspace;
    SubspaceIdx = find(ismember(circuitnames,circuitnamesInSubspace));
    [tuning_index_in_circuitSubspace, tuning_setup] = find_index_function(circuitnamesInSubspace);


    %% set up bounds and starting point for tuning factors
    % use tuning_factors_start to locate initially the center (median) 
    % of the posterior in the center of the viable space.
    
    PIdxCircuit = find(ismember(paramSpecs.names,circuitnames));   
    bminCircuit = paramSpecs.bmin(PIdxCircuit);
    bmaxCircuit = paramSpecs.bmax(PIdxCircuit);
    bminCircuit(islog) = log10(bminCircuit(islog));
    bmaxCircuit(islog) = log10(bmaxCircuit(islog));
    bminToTune = bminCircuit(SubspaceIdx(tuning_index_in_circuitSubspace));
    bmaxToTune = bmaxCircuit(SubspaceIdx(tuning_index_in_circuitSubspace));
    tuning_factors_start = zeros(1,length(tuning_index_in_circuitSubspace));
    
    for ipara = 1:length(tuning_index_in_circuitSubspace)
        t_idx = tuning_index_in_circuitSubspace(ipara);
        if tuning_setup(ipara) == -1 %only decrease
            bminToTune(ipara) = bminToTune(ipara) - min(posterior.V(:,t_idx)); 
            bmaxToTune(ipara) = 0;
            tuning_factors_start(ipara) = min(0, median(OutV.V(:,t_idx)) - median(posterior.V(:,t_idx))); % tuning factors in log space                                                
        elseif tuning_setup(ipara) == 1 %only increase
            bminToTune(ipara) = 0; % we can only increase tuning parameters
            bmaxToTune(ipara) = bmaxToTune(ipara) - max(posterior.V(:,t_idx));
            tuning_factors_start(ipara) = max(0, median(OutV.V(:,t_idx)) - median(posterior.V(:,t_idx))); % tuning factors in log space                        
        else %both allowed
            bminToTune(ipara) = bminToTune(ipara) - min(posterior.V(:,t_idx)); 
            bmaxToTune(ipara) = bmaxToTune(ipara) - max(posterior.V(:,t_idx));
            tuning_factors_start(ipara) = median(OutV.V(:,t_idx)) - median(posterior.V(:,t_idx)); % tuning factors in log space                    
        end
    end

    %% tune the feasibility by optimizing the tuning factors
    
    % define the problem in MEIGO
    % we minimize a cost = 1 - feasibility.
    problem = [];
    opts = [];

    problem.x_L= bminToTune;
    problem.x_U= bmaxToTune;
    problem.vtr= 0.01;
    problem.x_0 = tuning_factors_start;
    opts.tolc = 1e-3;
    opts.iterprint = 1;                                
    opts.local.solver  = 'fminsearch'; 
    opts.local.iterprint = 0;
    opts.local.bestx  = 1;
    opts.local.tol    = 3;  
    opts.ndiverse = 100;
    opts.maxeval = 20000;  
    opts.local.n1  = 10;
    opts.local.n2  = 10; 
    opts.local.finish  = 0;
    opts.maxtime = 100000;

    
    % initialize cost function
    allSamplesFlag = false; % we use a random subset of the posterior samples to make the optimization more efficient
    calc_cost_feasibility(tuning_factors_start, tuning_index_in_circuitSubspace, ...
        OutV, posterior,allSamplesFlag, mexNames, expData, paramNames, ...
        islog, fixedParamValuesTail, simopts, circuitnames);

    problem.f= 'calc_cost_feasibility';
    
    Results = MEIGO(problem,opts,'ESS');
    tuning_factors = Results.xbest;
    
    %% recompute the best feasibility with all the samples
    
    allSamplesFlag = true; 
    calc_cost_feasibility(tuning_factors, tuning_index_in_circuitSubspace, ...
        OutV, posterior,allSamplesFlag, mexNames, expData, paramNames, ...
        islog, fixedParamValuesTail, circuitnames);
    
    [costfeasibility,  idx_viable] = calc_cost_feasibility(tuning_factors);

    max_feasibility = 1 - costfeasibility;
                                
        
end