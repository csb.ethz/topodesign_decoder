% © 2021, ETH Zurich
function [paraoptlist, costlist] = find_initial_points_fit(update_date, paramNames_posterior, FlMode)


    
    
    paramSpecs = readtable('source/models/paramSpecs.txt');
    PIdx =  find(ismember(paramSpecs.names,paramNames_posterior)); % fit all                   
    paraest = paramSpecs.p0; 
    


    noise = true;
    disp('Initialize cost function')
    [~,~,~,~,ndata] = cost_decoder(paraest, paramSpecs, FlMode, PIdx, noise);
    close all;
    
    %% bounds   

    bmin = paramSpecs.bmin(PIdx);
    bmax = paramSpecs.bmax(PIdx) ;
    islog = paramSpecs.islog(PIdx) ;
    

    %% initial points
    
    npoints = 20;    
    lhsmatrix = lhsdesign(npoints,length(PIdx));
    paraoptlist = ones(npoints,length(PIdx));
    costlist = 1000*ones(npoints,1);
    npara = length(PIdx);
    
    % prepare matrix of initial points distributed within the parameter
    % bounds
    
    p0 = zeros(npoints,npara);

    for ipara = 1:npara
        if islog(ipara)
            p0(:,ipara) = 10.^(log10(bmin(ipara)) + (log10(bmax(ipara))-log10(bmin(ipara))).*(lhsmatrix(:,ipara)));
        else
            p0(:,ipara) = bmin(ipara) + (bmax(ipara) - bmin(ipara)).*(lhsmatrix(:,ipara));
        end
    end
    
    threshold = 0.5*chi2inv(0.95,ndata - length(PIdx));
    
    %% optimization
    
    if ~ismember('local',parallel.defaultClusterProfile)
        
        startPoolCluster(npoints);    
        parfor k = 1:npoints            
            cost_decoder(paraest, paramSpecs, FlMode, PIdx, noise); % initialize cost function
            funobj = 'cost_decoder';       
            % run optimization
            [paraoptlist(k,:), costlist(k)] = run_opt(funobj, islog, bmin, bmax, p0(k,:), threshold);
        end
    
    else
        for k = 1:npoints
        funobj = 'cost_decoder';       
        % run optimization
        [paraoptlist(k,:), costlist(k)] = run_opt(funobj, islog, bmin, bmax, p0(k,:), threshold);
        end
    
    end

    %% filter viable points
    
    idxBelowthreshold = find(costlist < 0.5*chi2inv(0.95,ndata - length(PIdx)));
    costlist = costlist(idxBelowthreshold);
    paraoptlist = paraoptlist(idxBelowthreshold,:);
    [~,idxmin] = min(costlist);
    paraopt = paraest;
    paraopt(PIdx) = paraoptlist(idxmin,:);
    save(['output_files/posterior/posterior' update_date '.mat'],'paraopt','costlist','paraoptlist', 'FlMode','PIdx');

end