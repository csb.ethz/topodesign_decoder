% © 2021, ETH Zurich
function [] = startPoolCluster(n)

% run from the code folder

    p = gcp('nocreate'); % If pool, do not create new one.
    if isempty(p)                
                
        if n >0
            cd '/usr/local/stelling/matlab_sge/sc03/R2019a+/'
            addpath(genpath(pwd)); 
            cd '~/Decoding_Circuit_Design/output_files/parallel_jobs'    
            parpool(n);
            cd '../../'
        end  

    
    end

end