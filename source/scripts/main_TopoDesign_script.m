% © 2021, ETH Zurich
% This main script follows the steps of figure S1 in the publication,
% describing the Topodesign method.

% We ran all the following functions. Intermediate results were saved so every
% step can be repeated independently.

% Requires the following toolboxes:
% HyperSpace (https://gitlab.com/csb.ethz/HYPERSPACE)
% TopoFilter (https://gitlab.com/csb.ethz/TopoFilter)
% IQM Tools V1.2.2.2 (https://iqmtools.intiquan.com/)
% MEIGO (http://gingproc.iim.csic.es/meigom.html)

%% TopoDesign steps A and B - find and rank topologies
% ---------------

%% A1. run decoder_find_initialpoints to find initial viable points
%intensive. Run on a cluster if possible.
% --> points were saved as 'code\needed_to_run_topofilter\initialpointslist_decoder.mat'

output_filename = 'initialpointslist_decoder_2'; 
% I added a 2 to make sure you do not replace the initial point list used in the study
decoder_find_initialpoints(output_filename);

%% A2. run decoder_run_TopoFilter to find potentially functional topologies (in our case 225)
%very intensive. Run on a cluster if possible.

% --> the output was saved in 'output_files\TopoFilter_output_samples\190927_172938'

decoder_run_TopoFilter;

%% B1. for every topology iTopo in that folder, run RankTopologies 

% find last finished topofilter run and clean up output folder
[topofilter_run, nTopologies] = find_last_finished_topofilter_run();

% change topofilter_run if you prefer to use a previous TopoFilter run
% We obtained nTopologies = 225 topologies.

% I submitted jobs individually to our cluster with qsub -t 1-nTopologies:1 qsubmit_parallel_190927.sh
% --> the output of RankedTopologies are one OutV.mat file per topology with all the metrics and viable spaces
% saved in 'output_files\ranked_topologies\190927_172938'

% Here we put it with a for loop, even if it is too intensive:

for iTopo = 1:nTopologies
    RankTopologies(iTopo, topofilter_run);
end

%% B2. gather all results by running RankedCircuits = gather_ranking_results('190927_172938')
% It will summarize them in a single structure RankedCircuits saved in 'output_files\ranked_topologies\190927_172938' 
% that keeps only functional topologies (in our case 109)
% 
topofilter_run = '190927_172938';
gather_ranking_results(topofilter_run);

%% TopoDesign step C: measure parts with flow cytometry 
%and gather data as .mat files in the folder output_files/data

%% TopoDesign steps D & E - compute parameter posterior & update ranking
% ---------------

%% D1. find initial points fitting all experimental data
%very intensive. Run on a cluster if possible.

update_date = datestr(now, 'yymmdd'); % save the new points found with a new update date
% We used update_date = '191003'; 

paramNames_posterior = {'d1';'d1tag';'kd1';'kL1';'n1';...
'd2';'kL2';'n2';'k12GFP';'Km12GFP';'Km21GFP';'k2aGFP';'Kma2GFP';...
'kLa';'na';'nad';'Kmad';'Kdtet'; 'nMperUnit'; 'kact_induced'};
% list of parameters concerned by measured parts and which would enable to
% build 69 of the 109 functional circuits.

% In order to have only one Km for each transcription factor(1, 2 or a), 
% we choose to estimate all the FP relative parameters and set others equal to these. 
% no parameters relative to tagged TetR that was not part of experiments. 

FlMode = 2:11; % select which experiments to use in the fit. Here cases 2 to 12 
% in the cost_decoder function correspond to all experiments performed

find_initial_points_fit(update_date, paramNames_posterior, FlMode);

% the optimal parameters which led to a cost below the chi-square threshold
% were saved as output_files/posterior/posterior191003.mat
%% D2. compute the parameter posterior
%very intensive. Run on a cluster if possible.

update_date = '191003';
step = 1; %step to start from in Hyperspace. We can jump to a later step if the earlier computations have already been done.
% 1: starting from MCexp. 2: if MCexp already done, starting from ellipsoid sampling; 3: if MCexp and ELexp already done, starting from uniform sampling Volint
compute_posterior(update_date, step);

% the posterior will be saved to the same file as optimal parameters.
% in our case output_files/posterior/posterior191003.mat


%% D1 and D2 initially done on module 6 only, no alpha
% to get an estimate of an ideal posterior distribution variance

posterior_suffix = '_module6_noalpha';
paramNames_posterior = {'d2','kL2','n2','k12FP','Km21FP','Kdtet','nMperUnit'};
% only tet related parameters
FlMode = 12; % only module 6 without alpha factor
find_initial_points_fit(posterior_suffix, paramNames_posterior, FlMode);
step = 1;
compute_confidence_region(posterior_suffix, step);

% output file was saved as output_files/posterior/posterior_module6_noalpha.mat

%% E. update ranking

% run again the RankTopologies function
% I submitted again jobs individually to our cluster with qsub -t 1-nTopologies:1 qsubmit_parallel_190927.sh
% --> If a posterior was saved in 'output_files/posterior/', 
% RankedTopologies will update every OutV.mat file in 
% 'output_files\ranked_topologies\190927_172938' with the new 
% feasibility based on the parameter posterior

% for example for topology 1:
topofilter_run = '190927_172938';
iTopo = 1;
RankTopologies(iTopo, topofilter_run);
RankedCircuits = gather_ranking_results(topofilter_run);

%% TopoDesign step F
% We assembled topology 39 whose features are gathered in 'output_files\ranked_topologies\190927_172938\OutV_78' 
% and in the general
% 'output_files\ranked_topologies\190927_172938\RankedCircuits.mat'

%% For simple motifs a posteriori analysis (not in the TopoDesign pipeline but included in the paper)
run_decoder_motifs;

