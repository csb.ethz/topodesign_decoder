% © 2021, ETH Zurich
function [paraopt, cost] = run_opt(funobj, islog, bmin, bmax, p0, threshold)

    problem = [];
    opts = [];
    problem.f=funobj;
    problem.x_L= bmin;
    problem.x_U= bmax;
    problem.vtr= threshold;
    problem.x_0 = p0;
    opts.tolc = 1e-1;
    opts.iterprint = 1;
    % opts.ndiverse = length(paramtofit);
    opts.ndiverse = 10;
    opts.maxeval = 10000;
    opts.maxtime = 20000;
    opts.local.solver  = 'fminsearch'; 
    opts.local.iterprint = 0;
    opts.local.n1  = 10;
    opts.local.n2  = 5; 
    opts.local.finish  = 0;
    opts.local.bestx  = 1;
    opts.local.tol    = 2;
    opts.log_var = find(islog); 


    Results = MEIGO(problem,opts,'ESS');

    cost = Results.fbest;
    paraopt = Results.xbest;
    
end
