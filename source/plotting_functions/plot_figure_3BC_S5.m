% © 2021, ETH Zurich
function [] = plot_figure_3BC_S5()

    foldername = 'plots/figure_3BC_S5';
    if ~exist(foldername)
        mkdir(foldername)
    end
    update_date = '191003';
    posterior = load(['output_files\posterior\posterior' update_date '.mat']);
    paramSpecs = readtable('source/models/paramSpecs.txt');
    
    close all;
    [~, ~, ~, ~, ndata] = cost_decoder(posterior.paraopt, paramSpecs, posterior.FlMode, posterior.PIdx);
    threshold = chi2inv(0.95,ndata-length(posterior.PIdx));
    fprintf('chi2 threshold %.2f \n',threshold);
    FigList = findobj('Type','Figure');
    maxbounds = fliplr([0.4 0.4 0.5 0.5 0.4 0.6 0.6 2 2  3  3 3 3 0.4  0.4  3  3  3  3]);
    
    for kfig = 1:length(FigList)
        fignum = FigList(kfig).Number;
        currentfig = figure(fignum);
        Subplots = currentfig.Children;
        for ksp = 1:length(Subplots)
            Subplots(ksp).YLim = [0 maxbounds(kfig)];
        end
        ax =  Subplots(1);    
        figtitle = ax.Title.String;    
        set(findall(gcf,'-property','FontSize'),'FontSize',14)
        set(gcf,'PaperOrientation','portrait','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
        print(sprintf(['plots/figure_3BC_S5/' figtitle ],fignum), '-dpdf');   

    end


end