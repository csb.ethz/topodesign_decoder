% © 2021, ETH Zurich
function [] = plot_figure_S7()
loaded = load('output_files\ranked_topologies\190927_172938\OutV_78.mat');
OutV = loaded.OutV;
paramSpecs = readtable('source/models/paramSpecs.txt');
PIdx = find(ismember(paramSpecs.names,OutV.circuitnamesInSubspace));
figure();
plot_OutV(OutV,paramSpecs(PIdx,:))
set(gcf,'PaperOrientation','portrait','PaperUnits','Normalized','PaperPosition',[0 0 1 0.7]);
print('plots/figure_S7', '-dpdf'); 

end