% © 2021, ETH Zurich
%% Single-pulse simulation for decoder

function [t,y] = pulse_decoder(odefun,tpulse,dpulse,tmax,para,y0,idxstate)

[t1,y1] = ode15s(odefun,[0 tpulse],y0(end,:)');
[~]     = odefun([],[],para,1,idxstate);
if dpulse > 0
    [t2,y2]  = ode15s(odefun,[0 dpulse],y1(end,:)');
else 
    y2 = y1;
end
[~]     = odefun([],[],para,0,idxstate);
[t3,y3] = ode15s(odefun,[0 tmax-(dpulse+tpulse)],y2(end,:)');

if dpulse > 0
    y = [y1;y2;y3];
    t = [t1; t2+tpulse; t3+tpulse+dpulse];
else
    y = [y1;y3];
    t = [t1; t3+tpulse+dpulse];
end    

return