% © 2021, ETH Zurich
function [] = plot_figure_1E()

    loaded = load('output_files/ranked_topologies/190927_172938/RankedCircuits.mat');
    RankedCircuits = loaded.RankedCircuits;

    %% simulate circuit 41
    topo_ids = [20 74];
    for ktopo = 1:length(topo_ids)
        topo_id = topo_ids(ktopo);
        idx = find([RankedCircuits.publication_id] == topo_id);
        OutV_id = RankedCircuits(idx).OutV_id;
        fprintf('parameters for circuit %d \n',topo_id)
        disp(RankedCircuits(idx).circuitnames);
        loaded = load(sprintf('output_files/ranked_topologies/190927_172938/OutV_%d.mat',OutV_id));
        OutV = loaded.OutV;
        idx_sample = randi(length(OutV.V));
        SData = sim_OutV(OutV,idx_sample);
        Citrine = [SData.goal.FP_0 SData.goal.FP_10 SData.goal.FP_20 SData.goal.FP_30 SData.goal.FP_40 SData.goal.FP_80 SData.goal.FP_120 SData.goal.FP_180 SData.goal.FP_360 ];
        figure()        
        h = plot((SData.goal.tspan- SData.goal.ind_time)./60,Citrine);
        xlim([0 7])
        cmap = flipud(parula(10));
        set(h, {'color'}, num2cell(cmap(2:10,:),2));
        legend({'0' '10' '20' '30' '40' '80' '120' '180' '360'});
        xlabel('time (h)')
        ylabel('Citrine')   
        set(gca,'TickLength',[0 0]);
        set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
        print(sprintf('plots/figure_1E_topo_%d',topo_id), '-dpdf'); 

    end

end