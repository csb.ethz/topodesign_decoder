% © 2021, ETH Zurich
% script generating figures in the publication

if ~isfolder('plots')
    mkdir('plots');
end

%% figure 1D + S2: topology ranking with ideal feasibility
plot_figure_1D_S2;

%% figure 1E : simulating example topologies
% We select a random viable sample for simulation so each time
% we plot this figure the result will be similar but not exactly the same
plot_figure_1E;

%% figure 2: simulating internal states for simple topologies
% We select a random viable sample for simulation so each time
% we plot this figure the result will be similar but not exactly the same
plot_figure_2;

%% figure 3BC and S5: plot best fit on data
plot_figure_3BC_S5;

%% figure 3E and S6: parameter posterior
plot_figure_3E_S6;

%% figure 4B: update ranking
plot_figure_4B;

%% figure 4D: tune parameter posterior
plot_figure_4D;

%% figure 5B: FACS population data at 100nM from figure 5B
plot_figure_5B

%% figure 5C: pulse duration assay
plot_figure_5C;

%% figure 6AC_S9B: aTc optimization
plot_figure_6AC_S9A;

%% figure 6B: feasibility region
plot_figure_6B_S9B;

%% figure 6D: FACS population data (at aTcopt) and prediction from fig 6D
plot_figure_6D;

%% figure S3: simple motif analysis
plot_figure_S3;

%% figure S4: parameter specifications
plot_figure_S4;

%% figure S7: viable space topology 39
plot_figure_S7;

%% figure S8: feasibility region with all tuning factors
plot_figure_S8;

%% additional not in paper due to lack of space

% figure S11: FACS data to check normalization by FSC-A correct
plot_figure_S11;

% figure S12: posterior estimated only with module 6, no alpha
% in order to estimate the variance of an ideal posterior
plot_figure_S12;


