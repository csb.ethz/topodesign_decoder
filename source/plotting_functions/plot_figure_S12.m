% © 2021, ETH Zurich
function [] = plot_figure_S12()

loaded = load('output_files\posterior\posterior_module6_noalpha.mat');
OutV = loaded.OutV;
PIdx = loaded.PIdx;
paramSpecs = readtable('source/models/paramSpecs.txt');
islog = find(paramSpecs.islog(PIdx));
bounds = [paramSpecs.bmin(PIdx) paramSpecs.bmax(PIdx)];
names = paramSpecs.names(PIdx);

figure()
logbounds = bounds;
logbounds(islog,:) = log10(bounds(islog,:));
for k = 1:length(PIdx)
    subplot(2,4,k)
    pd = fitdist(OutV.V(:,k),'Kernel');
    X = logbounds(k,1):0.1:logbounds(k,2);
    Y = pdf(pd,X);
    plot(X,Y,'Color','black','LineWidth',2);
    if k == 3 
        xlabel(names{k});
    else
    xlabel(['Log_{10}(' names{k} ')']);
    end
    xlim(logbounds(k,:));
    set(gca,'FontSize',14);
end

set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
set(gca,'TickLength',[0 0]);
print('plots/figure_S12', '-dpdf'); 


end