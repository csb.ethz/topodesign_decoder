% © 2021, ETH Zurich

function SData  = simulate_alpha_release(dataRelease, SData, EIdx, P, modelname, ICRef, PARAName, FlPlot, icase, modulename, minstd)
            
                
            % prepare simulated data structure
            dose = dataRelease.alphadose;
            idx_0 = (dose == 0);
            dose(idx_0) = 10^(-0.5); % to avoid problems with log
            SData.(EIdx).dose = dose;          
            timepoints = dataRelease.timepoints;
            SData.(EIdx).timepoints = timepoints;
            ntimepoints = length(dataRelease.timepoints);  
            ndose = length(dose);  
            
            SData.(EIdx).FP =  zeros(ntimepoints,ndose);
            SData.(EIdx).std = zeros(ntimepoints,ndose);
            SData.(EIdx).FRYname = modulename;
            
            % set P.alphamax in the objective function
        
            data = dataRelease;
% 			idx_alpha1000 = find(dataRelease.controlalphadose == 1000); %even the max alpha dose here is 5000 we use 1000 for consistency with alphaDR
            idx_alpha1000 = find(dataRelease.controlalphadose == 0); %even the max alpha dose here is 5000 we use 1000 for consistency with alphaDR
            
            idx_time0h = find(dataRelease.timepoints == 0);		 % steady state of Act1Citrine in absence of alpha	
            meancontrol0 = dataRelease.FRY2932.means(idx_time0h,idx_alpha1000);
			stdcontrol0 = dataRelease.FRY2932.std(idx_time0h,idx_alpha1000);
            meancontrolAct1 = dataRelease.FRY2908.means(idx_time0h,idx_alpha1000);			
            SData.(EIdx).x_scal = P.rho*(P.k_maturation + P.rho)/P.k_maturation*(meancontrolAct1 - meancontrol0); % scaling factor (Fact - F0)*rho*(km+rho)/km        
                 
                       
            
            % induce at t = induction_time with estradiol, aTc and alpha factor
            % dose
            
            P.est = 5000;  
            P.ind_time = 720;
            P.ind_duration = 720; % induce for 12h
            exp_tspan = 0:1:timepoints(end);
            tspan = [0:10:(P.ind_time + P.ind_duration - 4) (P.ind_time + P.ind_duration - 3):1:(P.ind_time + P.ind_duration - 1) P.ind_time + P.ind_duration + exp_tspan ]; % try to use as little data points as possible to make it faster
            % initialize simulated results matrix 
            FPContinuousOutput = zeros(length(tspan),ndose);
            LexAContinuousOutput = zeros(length(tspan),ndose);
                
            for kdose = 1:ndose
                
                P.alphamax = dose(kdose);
                
                para = fconvpara(P);   
                                
                output = IQMPsimulate(modelname, tspan , ICRef, PARAName, para);
                                                
                indexFP = ismember(output.states, 'FP');  
                indexLexA = ismember(output.variables, 'TF1on'); 

                idxtime = find(ismember(tspan - P.ind_time - P.ind_duration,timepoints));
                
                
                % report simulated data and normalized experimental data 
                FPContinuousOutput(:,kdose) = SData.(EIdx).x_scal*output.statevalues(:,indexFP);
                SData.(EIdx).FP(:,kdose) = meancontrol0 + FPContinuousOutput(idxtime,kdose);
                LexAContinuousOutput(:,kdose) = SData.(EIdx).x_scal*output.variablevalues(:,indexLexA);
                SData.(EIdx).LexA(:,kdose) = LexAContinuousOutput(idxtime,kdose);
                SData.(EIdx).means(:,kdose) = data.(modulename).means(1:ntimepoints,kdose);      
                              % error propagation when substracting control
                SData.(EIdx).std(:,kdose) = max([minstd*ones(ntimepoints,1); sqrt(data.(modulename).std(1:ntimepoints,kdose) + stdcontrol0).^2]);   
  
            end              
            
            if FlPlot     
                figure(1000 + icase + P.tc)
                clf;
                
                subplot(2,1,2);
                map = parula(ndose + 2);
                map = flipud(map(2:(ndose+1),:));
                h = plot((tspan(idxtime(1):end) - P.ind_time - P.ind_duration)./60,FPContinuousOutput(idxtime(1):end,:));
                set(h, {'color'}, num2cell(map,2));
                colormap(map);

                hold on;

                for kdose = 1:ndose                    
                    errorbar(timepoints./60,SData.(EIdx).means(:,kdose),SData.(EIdx).std(:,kdose),'o','LineWidth',0.75,'color',map(kdose,:),'CapSize',0,'MarkerSize',15);
                    hold on;
                end
                xlabel('time after induction (h)')
                ylabel(['Fluorescence of ' modulename])  
                yLim = get(gca,'YLim');
                set(gca,'YLim', [0 yLim(2)],'TickLength',[0 0]);
                    
                    
                subplot(2,1,1)
                title(EIdx,'Interpreter','none')
                    
                hold on;
                for kdose = 1:ndose                    
                    errorbar(log10(dose(kdose)),SData.(EIdx).means(1,kdose),SData.(EIdx).std(1,kdose),'o','LineWidth',0.75,'color',map(kdose,:),'CapSize',0,'MarkerSize',15);
                    hold on;

                end
                plot(log10(dose),FPContinuousOutput(idxtime(1),:),'-','color',[0.3 0.3 0.3],'LineWidth',0.75);
                xlabel('log alpha (nM)')
                xlim([-0.5 3])
                ylabel('Steady State Fluorescence at 0h')
                yLim = get(gca,'YLim');
                set(gca,'YLim', [0 yLim(2)],'TickLength',[0 0]);
                     

            end
            
end            