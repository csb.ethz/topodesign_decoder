% © 2021, ETH Zurich
function [] = decoder_find_initialpoints( output_filename )

    % find a set of initial points that achieve the decoder behavior to start topofilter from. 
    % set saveflag = true if you want to save the output.
    
    %% define initial points with latin hypercube

    npoints = 100;
    paramSpecs = readtable('models/paramSpecs.txt');
    notProjected = paramSpecs.names;
    npara = length(notProjected);
    parainit = zeros(npoints,npara);
    paraoptlist = zeros(npoints,npara);
    costlist = 1000*ones(npoints,1);
    islog = paramSpecs.islog;
    lhsmatrix = lhsdesign(npoints,npara);
    bmin = paramSpecs.bmin;
    bmax = paramSpecs.bmax;


    for i = 1:npoints
        for p = 1:npara
            if islog(p) == 1
                parainit(i,p) = 10^(lhsmatrix(i,p)*(log10(bmax(p)) - log10(bmin(p))) + log10(bmin(p)));
            else
                parainit(i,p) = lhsmatrix(i,p)*(bmax(p) - bmin(p)) + bmin(p);
            end
        end
    end

    %% start parallel pool
    
        
    

    %% initialize objective function and run optimization in parallel

    paraopt = paramSpecs.p0';
    FlMode = 1;
    PIdx = 1:length(paraopt);
    threshold = 0.5;

    if any(ismember(parallel.clusterProfiles,'SGE_BSSE'))
        startPoolCluster(npoints);
        parfor ipoint = 1:npoints

            cost_decoder(paraopt, paramSpecs, FlMode, PIdx);
            close all;
            funobj = 'cost_decoder';

            [paraoptlist(ipoint,:), costlist(ipoint,:)] = run_opt(funobj, islog, bmin, bmax, parainit(ipoint,:), threshold)    
        end
        
    else
        
        for ipoint = 1:npoints

            cost_decoder(paraopt, paramSpecs, FlMode, PIdx);
            close all;
            funobj = 'cost_decoder';

            [paraoptlist(ipoint,:), costlist(ipoint,:)] = run_opt(funobj, islog, bmin, bmax, parainit(ipoint,:), threshold)    
        end
    end
        

    idxBelowthreshold = find(costlist < 1);
    costlist = costlist(idxBelowthreshold);
    paraoptlist = paraoptlist(idxBelowthreshold,:);

    if numel(idxBelowthreshold) > 20       
        save(['source/needed_to_run_topofilter/' output_filename '.mat'],'paraoptlist','costlist');
    end
       
end