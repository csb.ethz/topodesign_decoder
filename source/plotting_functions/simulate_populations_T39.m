% © 2021, ETH Zurich
function  Citrine_6h_aTcopt = simulate_populations_T39()

    circuit_id = 39;
    ncells = 1000;
    loaded = load('output_files/data/dataPulsealpha_aTc.mat');
    dataPulsealpha = loaded.dataPulsealpha;
    aTcdose = dataPulsealpha.aTcdose;

    % load posterior samples
    update_date = '191003';
    loaded = load(['output_files\posterior\posterior' update_date '.mat']);
    posterior = loaded.OutV;
    paramSpecs = readtable('source/models/paramSpecs.txt');
    paramSpecsNames = paramSpecs.names;
    
    tc_idx = find(ismember(paramSpecs.names,'tc'));
    PIdxPosterior = [loaded.PIdx; tc_idx]; %adding tc to the posterior indices
    posterior.V(:,end+1) = zeros(size(posterior.V,1),1); % to add a tc value which was not fitted but always tunable

    % load circuit viable points
    loaded = load('output_files/ranked_topologies/190927_172938/RankedCircuits.mat');
    RankedCircuits = loaded.RankedCircuits;
    idx = find([RankedCircuits.publication_id] == circuit_id);
    OutV_id = RankedCircuits(idx).OutV_id;
    loaded = load(sprintf('output_files/ranked_topologies/190927_172938/OutV_%d.mat',OutV_id));
    OutV = loaded.OutV;


    circuitnamesInSubspace = OutV.circuitnamesInSubspace;
    ndimcircuit = length(circuitnamesInSubspace);
    PosteriorNames = paramSpecsNames(PIdxPosterior);
    index_in_PosteriorNames = zeros(1,ndimcircuit);

    copynumbers_idx = find(cellfun(@isempty,regexp(circuitnamesInSubspace,'k\d\w\w'))==0);
    for iparaCircuitSubspace = 1:ndimcircuit            
        if length(circuitnamesInSubspace{iparaCircuitSubspace}) > 6
            index_in_PosteriorNames(iparaCircuitSubspace) = find(cellfun(@isempty,regexp(PosteriorNames,[circuitnamesInSubspace{iparaCircuitSubspace}(1:3) '\w\w\w']))==0);
        elseif ismember(iparaCircuitSubspace,copynumbers_idx)
            index_in_PosteriorNames(iparaCircuitSubspace) = find(cellfun(@isempty,regexp(PosteriorNames,[circuitnamesInSubspace{iparaCircuitSubspace}(1:3) '\w\w']))==0);
        else
            index_in_PosteriorNames(iparaCircuitSubspace) = find(ismember(PosteriorNames,circuitnamesInSubspace{iparaCircuitSubspace}));
        end                        
    end             


    posterior_circuitSubspace = [];
    posterior_circuitSubspace.V = posterior.V(:,index_in_PosteriorNames);        
    ndimCircuit = length(OutV.circuitnamesInSubspace);
    factors = zeros(1,ndimCircuit);
    find_index_function = @find_tuning_idx_k2a_tc;     
    [tuning_index_in_circuitSubspace, tuning_setup] = find_index_function(circuitnamesInSubspace);

    

    %%
    Km1idx = find(~contains(OutV.circuitnames,'Km1')==0);
    Km2idx = find(~contains(OutV.circuitnames,'Km2')==0);
    Kmaidx = find(cellfun(@isempty,regexp(OutV.circuitnames,'Kma\d'))==0);
    KmIdx = {Km1idx; Km2idx; Kmaidx};
    KmIdxVector = [Km1idx(2:end);Km2idx(2:end);Kmaidx(2:end)];
    SubspaceIdx = setdiff(1:length(OutV.circuitnames)',KmIdxVector);

    % Define which parameters are projected  
    notProjected = OutV.circuitnames;
    PIdx = find(ismember(paramSpecs.names,notProjected));
    FlMode = 1;
    islog = paramSpecs.islog(PIdx);

    ndoses = length(aTcdose);
    Citrine_6h_aTcopt = zeros(ncells,3);    
    

    %aTcopt
    
    tuning_factors = OutV.updated_tuning_factors_wo_k12;
    factors(tuning_index_in_circuitSubspace) = tuning_factors;
    moved_posterior = [];
    moved_posterior.V = posterior_circuitSubspace.V + factors;

    ncells = 1000;
    for kposterior_sample = 1:ncells
        fprintf('aTc opt, simulating cell %d/%d \n', kposterior_sample,ncells);
        ParamValuesInSubspace = moved_posterior.V(kposterior_sample,:);
        variableParamValues = zeros(length(OutV.circuitnames),1);

        variableParamValues(SubspaceIdx) = ParamValuesInSubspace;

        for iKm = 1:3
            u = KmIdx{iKm};
            if ~isempty(u)
                for k = 2:length(u)
                ref = u(1);
                idx = u(k);
                variableParamValues(idx) = ParamValuesInSubspace(ref);
                end
            end
        end

        % parameters are log. map them back to linear space
        variableParamValues(find(islog)) = 10.^variableParamValues(find(islog));

        % create the full estimated parameter set that incorporates projected and
        % non-projected values
        paraest = paramSpecs.projections;
        paraest(PIdx) = variableParamValues;

        % simulate the full model with this parameter set
        [~,~,~,SData,~] = cost_decoder(paraest, paramSpecs, FlMode, PIdx);


        Citrine_6h_aTcopt(kposterior_sample,1) = SData.goal.FP_0(end);
        Citrine_6h_aTcopt(kposterior_sample,2) = SData.goal.FP_30(end);
        Citrine_6h_aTcopt(kposterior_sample,3) = SData.goal.FP_180(end);

    end
        
    save('output_files/prediction/predicted_data.mat','Citrine_6h_aTcopt');


end