% © 2021, ETH Zurich
function C = decoder_couplingMatrix()

    % initialise: no coupling
    C = [];
    
    for p = [6 9 17 20 23 26] % p is one of the AND gate promoters
        % coupling k_p to Km_p in one direction only - we can have fully constitutive promoters if both Km_p are projected but not k_p
        % we remove now constitutive promoters
        C{end+1,1} = p;
        C{end,2} = [p+1 p+2];
%         C(p,p+1) = true;
%         C(p,p+2) = true;
%         C(p+1,p) = ~ismember(p+2,parameterIdxs);
%         C(p+2,p) = ~ismember(p+1,parameterIdxs);
    end
    
    for p = [6 17 23] % p is one of the k12 AND gate promoters
        C{end+1,1} = p+1; % the repression can happen only if the promoter is induced.
        C{end,2} = p;
    end
    
    for p = [9 20 26] % p is one of the k2a AND gate promoters
        C{end+1,1} = p+2; % the repression can happen only if the promoter is induced.
        C{end,2} = p;
    end

    
    
    
    for p = [1 12] % p is degradation for TF1 or TF2
        %d1tag parameter is only used to fit data so here it is always
        %projected.
        C{end+1,1} = [];
        C{end,1} = p+1; % always project d1tag since it is not part of the model (only used for posterior estimation)                
        
        % coupling d_k, kd_k, kL, n to the two k_p producing TFk. 
        
        for i = [p p+3 p+4] % if any of d_k, kL, n is projected, project the two k
            C{end+1,1} = i;
            C{end,2} = [p+5 p+8];
        end
        
        C{end+1,1} = [p+5 p+8];    % If no TFk is produced then remove all other parameters.   
        C{end,2} = p:(p+10);  
        
        %  if both kd are projected then project Kmad and nad. 
        C{end+1,1} =  [3 14]; 
        C{end,2} = [31 32];        
        
        % If Kmad or nad is projected then project both kd.
        for i = [31 32]
            C{end+1,1} =  i;
            C{end,2} = [3 14];                
        end
    end
    
    % project Kdtet and tetracycline and nMperUnit together
    C{end+1,1} = 33;
    C{end,2} = [34 35];
    C{end+1,1} = 34;
    C{end,2} = 33;
    C{end+1,1} = 35;
    C{end,2} = 33;
    
    % always project 
    C{end+1,1} = [];
    C{end,1} = 36; % always project kact_induced since it is not part of the model (only used for posterior estimation)
       
end
