% © 2021, ETH Zurich
function [] = plot_figure_4B()

% load ranked circuits
loaded = load('output_files/ranked_topologies/190927_172938/RankedCircuits.mat');
RankedCircuits = loaded.RankedCircuits;
update = 'updated';

plot_robustness_feasibility(RankedCircuits, update)

% 40 circuits include a post-transl interaction on TF2 and for these
% updated feasibility was not computed, instead it was set to -1.
% they will not appear on the figure by setting bounds above zero.

ylim([0 0.6])

%% figure 1 D
set(gca,'FontSize',18);
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
set(gca,'TickLength',[0 0]);
print('plots/figure_4B','-dpdf')


end