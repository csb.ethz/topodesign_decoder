% © 2021, ETH Zurich
function [] = plot_figure_5B()

loaded = load('output_files/data/dataPulsealpha_aTc.mat');
dataPulsealpha = loaded.dataPulsealpha;

figure()
npulses = length(dataPulsealpha.alphapulse);
map = lines(npulses);




strain = 'FRY2958';
data = dataPulsealpha.LogNormalizedCitrine_gated.(strain);

kdose = 5; %at 100 nM
for kalphapulse = 1:npulses
    pd = fitdist(data{kdose,kalphapulse},'Kernel');
    X = -3:0.05:1;
    Y = pdf(pd,X);
    plot(X,Y,'Color',map(kalphapulse,:),'LineWidth',2);
    set(gca,'FontSize',20);
    hold on;
end

xlim([-3 1])  
ylim([0 3.5])      
xlabel('Normalized fluorescence')   



legendCell = strcat(string(num2cell(dataPulsealpha.alphapulse)),'min');
legend(legendCell);
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
print('plots/figure_5B', '-dpdf'); 


end