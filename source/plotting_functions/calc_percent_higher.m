% © 2021, ETH Zurich
function [percent_higher, quantile_C3] = calc_percent_higher(dataPulsealpha)

    strains = dataPulsealpha.strains;
    
    prob = 0.95; % value 10% highest cells

    kpulse3h = find(dataPulsealpha.alphapulse == 180);
    quantile_C3 = quantile(dataPulsealpha.LogNormalizedCitrine_gated.FRY2952{1,kpulse3h},1 - prob);

    nstrains = numel(strains);
    ndose = numel(dataPulsealpha.aTcdose);
    npulses = length(dataPulsealpha.alphapulse);
    percent_higher = zeros(ndose,nstrains,3);

    for kstrain = 1:nstrains
        for kdose = 1:ndose   
            for kalphapulse = 1:npulses
                data = dataPulsealpha.LogNormalizedCitrine_gated.(strains{kstrain}){kdose,kalphapulse};
                percent_higher(kdose,kstrain,kalphapulse) = 100*numel(find(data > quantile_C3))/numel(data);
            end
        end
    end

end