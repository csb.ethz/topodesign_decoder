% © 2021, ETH Zurich
function [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise)

    % update cost with the residuals  from the experiment expname, and with
    % noise added to simulated values if noise = true
    
    
    if noise
        res = (SData.(expname).FP + normrnd(0,SData.(expname).std) - SData.(expname).means)./SData.(expname).std;
    else
        res = (SData.(expname).FP - SData.(expname).means)./SData.(expname).std;  
    end
    expnames{end+1} = expname;
    costvector(end+1) = sum(res(:).^2);
    ndata = ndata + length(res(:)); 
    
end