% © 2021, ETH Zurich
function  high_feasibility_region = tuning_factors_region(tuning_factors_opt, OutV, posterior_circuitSubspace, find_index_function, paramSpecs, mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames)

% function to find the high feasibility region. 
% will be run in parallel if started on the submit node of the cluster.
% similar to tune_feasibility, but instead of just maximizing feasibility
% we find points for which feasibility > 0.9* max feasibility.

% tuning factors provides indices of parameters to tune and tuning setup 
% (parameters allowed to increase, decrease or both)

% posterior = parameter posterior that can be the ideal gaussian or the
% approximate bayesian posterior estimated with data
% it has to be mapped to the circuit same Km subspace

% OutV = information on the topology including uniformly distributed
% viable points

    circuitnamesInSubspace = OutV.circuitnamesInSubspace;
    SubspaceIdx = find(ismember(circuitnames,circuitnamesInSubspace));
    [tuning_index_in_circuitSubspace, tuning_setup] = find_index_function(circuitnamesInSubspace);


    %% set up bounds for tuning factors
    
    PIdxCircuit = find(ismember(paramSpecs.names,circuitnames));   
    bminCircuit = paramSpecs.bmin(PIdxCircuit);
    bmaxCircuit = paramSpecs.bmax(PIdxCircuit);
    bminCircuit(islog) = log10(bminCircuit(islog));
    bmaxCircuit(islog) = log10(bmaxCircuit(islog));
    bminToTune = bminCircuit(SubspaceIdx(tuning_index_in_circuitSubspace));
    bmaxToTune = bmaxCircuit(SubspaceIdx(tuning_index_in_circuitSubspace));

    for ipara = 1:length(tuning_index_in_circuitSubspace)
        if tuning_setup(ipara) == -1 %only decrease
            bminToTune(ipara) = bminToTune(ipara) - min(posterior_circuitSubspace.V(:,tuning_index_in_circuitSubspace(ipara))); % ssume we can also decrease
            bmaxToTune(ipara) = 0;
        elseif tuning_setup(ipara) == 1 %only increase
            bminToTune(ipara) = 0; % we can only increase tuning parameters
            bmaxToTune(ipara) = bmaxToTune(ipara) - max(posterior_circuitSubspace.V(:,tuning_index_in_circuitSubspace(ipara)));
        else %both allowed
            bminToTune(ipara) = bminToTune(ipara) - min(posterior_circuitSubspace.V(:,tuning_index_in_circuitSubspace(ipara))); % ssume we can also decrease
            bmaxToTune(ipara) = bmaxToTune(ipara) - max(posterior_circuitSubspace.V(:,tuning_index_in_circuitSubspace(ipara)));
        end
    end
    
    %% explore the tuning factors space 
    % keep points for which the feasibility is > 0.9 updated_feasibility
    % for a cost = 1 - feasibility, the threshold becomes cost < 0.1 +
    % 0.9*updated_feasibility_cost

    % we use the functions of Hyperspace.


    plotFlag = false;
    allSamplesFlag = false; 

    npoints = 50;

    if ~ismember('local',parallel.defaultClusterProfile)
        startPoolCluster(npoints);
    end                
    
    % Monte Carlo sampling

    OutM = cell(1,npoints);
    calc_cost_feasibility(tuning_factors_opt, tuning_index_in_circuitSubspace, ...
        OutV, posterior_circuitSubspace,allSamplesFlag, ...
        mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);      
    updated_feasibility_cost = calc_cost_feasibility(tuning_factors_opt);

    if isempty(gcp('nocreate'))
        for k = 1:npoints
            calc_cost_feasibility(tuning_factors_opt, tuning_index_in_circuitSubspace, ...
            OutV, posterior_circuitSubspace,allSamplesFlag, ...
            mexNames, expData, paramNames, islog, fixedParamValuesTail,  simopts, circuitnames);                                         
            OutM{k} = MCexp('calc_cost_feasibility',0.1 + 0.9*updated_feasibility_cost, tuning_factors_opt, bmaxToTune', bminToTune', 200);
        end
    else
        parfor k = 1:npoints
            calc_cost_feasibility(tuning_factors_opt, tuning_index_in_circuitSubspace, ...
            OutV, posterior_circuitSubspace,allSamplesFlag, ...
            mexNames, expData, paramNames, islog, fixedParamValuesTail,  simopts, circuitnames);                                         
            OutM{k} = MCexp('calc_cost_feasibility',0.1 + 0.9*updated_feasibility_cost, tuning_factors_opt, bmaxToTune', bminToTune', 200);
        end
    end

    MCsamples = [];
    for k = 1:npoints
        MCsamples = [MCsamples; OutM{k}.V];
    end
    
    
    delete(gcp('nocreate'));
    
    % ellipsoid expansion sampling
    
    OutE = cell(1,1);

    if ~ismember('local',parallel.defaultClusterProfile)
        
        startPoolCluster(1);
        
        parfor k =1:1 % to avoid running on the submit node
        % initialize cost function;
            calc_cost_feasibility(tuning_factors_opt, tuning_index_in_circuitSubspace, ...
                OutV, posterior_circuitSubspace,allSamplesFlag, ...
                mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);                                         

            OutE{k} = ELexp('calc_cost_feasibility',0.1 + 0.9*updated_feasibility_cost, MCsamples, bmaxToTune', bminToTune', 10000);
        end
    else        
        % initialize cost function;
            calc_cost_feasibility(tuning_factors_opt, tuning_index_in_circuitSubspace, ...
                OutV, posterior_circuitSubspace,allSamplesFlag, ...
                mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);                                         

            OutE{k} = ELexp('calc_cost_feasibility',0.1 + 0.9*updated_feasibility_cost, MCsamples, bmaxToTune', bminToTune', 10000);
    end

    delete(gcp('nocreate'));
    startPoolCluster(npoints);
    
    % uniformly sampling inside the high feasibility region
    
    tuning_factors_OutVArray = cell(1,npoints);

    if isempty(gcp('nocreate'))
        for k = 1:npoints

        % initialize cost function;
        calc_cost_feasibility(tuning_factors_opt, tuning_index_in_circuitSubspace, ...
            OutV, posterior_circuitSubspace,allSamplesFlag, ...
            mexNames, expData, paramNames, islog, fixedParamValuesTail, circuitnames);  
        tuning_factors_OutVArray{k} = Volint('calc_cost_feasibility',0.1 + 0.9*updated_feasibility_cost, [MCsamples; OutE{1}.V], bmaxToTune', bminToTune', 200);                
        end
    
    else 
        parfor k = 1:npoints

        % initialize cost function;
        calc_cost_feasibility(tuning_factors_opt, tuning_index_in_circuitSubspace, ...
            OutV, posterior_circuitSubspace,allSamplesFlag, ...
            mexNames, expData, paramNames, islog, fixedParamValuesTail, circuitnames);  
        tuning_factors_OutVArray{k} = Volint('calc_cost_feasibility',0.1 + 0.9*updated_feasibility_cost, [MCsamples; OutE{1}.V], bmaxToTune', bminToTune', 200);                
        end
    end

    high_feasibility_region.V = [];
    high_feasibility_region.cost = [];
    for k = 1:npoints
        if isfield(tuning_factors_OutVArray{k},'V')
            high_feasibility_region.V = [high_feasibility_region.V; tuning_factors_OutVArray{k}.V];
            high_feasibility_region.cost = [high_feasibility_region.cost; tuning_factors_OutVArray{k}.cost];
        end
    end

end