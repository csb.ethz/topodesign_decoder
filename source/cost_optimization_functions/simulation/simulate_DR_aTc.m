% © 2021, ETH Zurich
function SData  = simulate_DR_aTc(dataDRaTc, SData, EIdx, P, modelname, ICRef, PARAName, FlPlot, icase, modulename, minstd)
                
            % prepare simulated data structure
            if ismember(modulename,'FRY2908')
                dose = 1e-2;
            else
                dose = 10*dataDRaTc.aTcdose;                
                idx_0 = (dose == 0);
                dose(idx_0) = 10^(-0.1); % to avoid problems with log
            end
            SData.(EIdx).dose = dose;          
            timepoints = dataDRaTc.timepoints;
            SData.(EIdx).timepoints = timepoints;
            ntimepoints = length(dataDRaTc.timepoints);  
            ndose = length(dose);  
            
            SData.(EIdx).FP =  zeros(ntimepoints,ndose);
            SData.(EIdx).std = zeros(ntimepoints,ndose);
            SData.(EIdx).FRYname = modulename;
            
            % set P.alphamax in the objective function
            idx_alpha = find(ismember(dataDRaTc.alphadose,P.alphamax));            
            data = dataDRaTc.alpha_array{idx_alpha};
            idx_alpha0 = find(dataDRaTc.alphadose == 0);
            idx_time6h = find(dataDRaTc.timepoints == 360);
            datacontrol0 = dataDRaTc.alpha_array{idx_alpha0}.FRY2932;
            datacontrolAct1 = dataDRaTc.alpha_array{idx_alpha0}.FRY2908;
            SData.(EIdx).x_scal = P.rho*(P.k_maturation + P.rho)/P.k_maturation*(datacontrolAct1.means(idx_time6h) - datacontrol0.means(idx_time6h)); 
            % scaling factor (Fact - F0)*rho*(km+rho)/km        
                 
                       
            
            % induce at t = induction_time with estradiol, aTc and alpha factor
            % dose
            
            P.est = 5000;  
            P.ind_time = 400;
            P.ind_duration = timepoints(end); % alpha factor step function 
            exp_tspan = 0:1:timepoints(end);
            tspan = [0:10:(P.ind_time - 4) (P.ind_time - 3):1:(P.ind_time - 1) P.ind_time + exp_tspan]; % try to use as little data points as possible to make it faster
            % initialize simulated results matrix 
            FPContinuousOutput = zeros(length(tspan),ndose);
                
            for kdose = 1:ndose
                
                P.tc = dose(kdose);
                
                para = fconvpara(P);   
                                
                output = IQMPsimulate(modelname, tspan , ICRef, PARAName, para);
                                                
                indexFP = ismember(output.states, 'FP');    

                idxtime = find(ismember(tspan - P.ind_time,timepoints));
                
                
                % report simulated data and normalized experimental data 
                FPContinuousOutput(:,kdose) = SData.(EIdx).x_scal*output.statevalues(:,indexFP);
                SData.(EIdx).FP(:,kdose) = datacontrol0.means(idx_time6h) + FPContinuousOutput(idxtime,kdose);
                SData.(EIdx).means(:,kdose) = data.(modulename).means(:,kdose);
                % error propagation when substracting control
                SData.(EIdx).std(:,kdose) = max([minstd*ones(ntimepoints,1); sqrt(data.(modulename).std(:,kdose)+datacontrol0.std(idx_time6h)).^2]);   
%                 SData.(EIdx).std(:,kdose) = max([minstd*ones(ntimepoints,1); data.(modulename).std(:,kdose)]);     
                
            end              
            
            if FlPlot     
                
                figure(icase + P.alphamax)
                clf;
                
                subplot(2,1,2);
                    map = parula(ndose + 2);
                    map = flipud(map(1:ndose,:));
%                     h = plot((tspan(idxtime(1):end) - P.ind_time)./60,FPContinuousOutput(idxtime(1):end,:));
                    h = plot((tspan - P.ind_time)./60,FPContinuousOutput);
                    set(h, {'color'}, num2cell(map,2));
                    colormap(map);                    
%                     c = colorbar( 'FontSize',11,'YTick',[-4 log2(dose(2:2:end))],'YTickLabel',[0 0.1*round(10*dose(2:2:end))]);
%                     c.Label.String = 'aTc (nM)';
%                     caxis(log2([dose(2) dose(end)]));

                    hold on;

                    for kdose = 1:ndose                    
                        errorbar(timepoints./60,SData.(EIdx).means(:,kdose),SData.(EIdx).std(:,kdose),'o','LineWidth',0.75,'color',map(kdose,:),'CapSize',0,'MarkerSize',15);
                        hold on;
                    end
                    xlabel('time after induction (h)')
                    ylabel(['Fluorescence of ' modulename])  
                    xlim([0 6])
                    yLim = get(gca,'YLim');
                    set(gca,'YLim', [0 yLim(2)],'TickLength',[0 0]);
                     
                    
%                     ylim([0 max(max(FPContinuousOutput(:)),max(SData.(EIdx).means(:) + SData.(EIdx).std(:)))])
%                     ylim([0 2.5]);

                    
                    
                subplot(2,1,1)
                title(EIdx,'Interpreter','none')
                    
                    hold on;
                    for kdose = 1:ndose                    
%                         errorbar(log10(dose(kdose)),SData.(EIdx).means(end,kdose),SData.(EIdx).std(end,kdose),'o','LineWidth',0.75,'color',map(kdose,:),'CapSize',0,'MarkerSize',15);
                        errorbar(log10(dose(kdose)),SData.(EIdx).means(end,kdose),SData.(EIdx).std(end,kdose),'o','LineWidth',0.75,'color','k','CapSize',0,'MarkerSize',15);                        
                        hold on;
                        
                    end
                    plot(log10(dose),FPContinuousOutput(end,:),'-','color',[0.3 0.3 0.3],'LineWidth',0.75);
                    xlabel('log aTc (nM)')
                    xlim([-0.1 log10(2500)])
                    ylabel('Fluorescence at 6h')  
                    yLim = get(gca,'YLim');
                    set(gca,'YLim', [0 yLim(2)],'TickLength',[0 0]);
                     
%                     ylim([0 max(max(FPContinuousOutput(:)),max(SData.(EIdx).means(:) + SData.(EIdx).std(:)))])
%                     ylim([0 2.5])

            end
            
end            