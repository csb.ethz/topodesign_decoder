function [tuning_index_in_circuitSubspace, tuning_setup] =  find_tuning_idx_copynumbers_tc(circuitnamesInSubspace)
    copynumbers_idx = find(cellfun(@isempty,regexp(circuitnamesInSubspace,'k\d\w\w'))==0);    
    tc_idx = find(ismember(circuitnamesInSubspace,'tc'));
    tuning_index_in_circuitSubspace = [copynumbers_idx; tc_idx];    
    tuning_setup = [ones(1,length(copynumbers_idx)) 2];
end