% © 2021, ETH Zurich
function [] = generateMEXfile(model,modelname)
    if ismember(parallel.defaultClusterProfile, {'local'})
        if ~exist([modelname '.mexw64'],'file') % a for cluster
            IQMmakeMEXmodel(model,modelname)
        end    
    else
        if ~exist([modelname '.mexa64'],'file')
            IQMmakeMEXmodel(model,modelname)
        end    
    end
end