% © 2021, ETH Zurich
function compute_posterior(update_date, step)


paramSpecs = readtable('source/models/paramSpecs.txt');


% fit only the FP relative parameters and set others equal to these

loaded = load(['output_files/posterior/posterior' update_date '.mat']); 
paraopt = loaded.paraopt;
paraoptlist = loaded.paraoptlist;
FlMode = loaded.FlMode;
PIdx = loaded.PIdx;
bmin = paramSpecs.bmin(PIdx);
bmax = paramSpecs.bmax(PIdx);
islog = paramSpecs.islog(PIdx);
islogindices = find(islog);

bminlog = bmin;
bminlog(islogindices) = log10(bmin(islogindices));

bmaxlog = bmax;
bmaxlog(islogindices) = log10(bmax(islogindices));

paraoptlistlog = paraoptlist;
paraoptlistlog(:,islogindices) = log10(paraoptlist(:,islogindices));
paraoptlog = paraoptlistlog(1,:);

%% use Hyperspace to find viable region first by Monte Carlo sampling then ellipsoid expansion

if step <= 1
    
    ninitpoints = size(paraoptlist,1);
    OutM = cell(1,ninitpoints);
    noise = true;
    
    % if possible in parallel
    
    if any(ismember(parallel.clusterProfiles,'SGE_BSSE'))
        startPoolCluster(ninitpoints);
        parfor k = 1:ninitpoints
            % we cannot use function handles in parfor loops        
            [~,~,~,~,ndata] = cost_decoder(paraopt, paramSpecs, FlMode, PIdx, noise);
            cost_decoder_log(paraoptlog,islog);
            close all;
            funlog = 'cost_decoder_log';
            fprintf('running MC exploration %d/%d \n',k,ninitpoints)
            OutM{k} = MCexp(funlog,chi2inv(0.95,ndata-length(PIdx)),paraoptlistlog(k,:),bmaxlog',bminlog',1000);
        end
        
    else
        % we can initialize outside the loop in that case.
        [~,~,~,~,ndata] = cost_decoder(paraopt, paramSpecs, FlMode, PIdx, noise);
        cost_decoder_log(paraoptlog, islog);
        close all;
            
        for k = 1:ninitpoints
            % we cannot use function handles in parfor loops                    
            funlog = 'cost_decoder_log';
            fprintf('running MC exploration %d/%d \n',k,ninitpoints)            
            OutM{k} = MCexp(funlog,chi2inv(0.95,ndata-length(PIdx)),paraoptlistlog(k,:),bmaxlog',bminlog',1000);
        end
    end

    %%
    MCsamples = [];
    for k = 1:size(paraoptlist,1)
        MCsamples = [MCsamples; OutM{k}.V];
    end
    
else
    MCsamples = loaded.MCsamples;
end
save(['output_files/posterior/posterior' update_date  '.mat'],  'MCsamples','-append');

%
if step <= 2
    noise = true;
    [~,~,~,~,ndata] = cost_decoder(paraopt, paramSpecs, FlMode, PIdx, noise);    
    cost_decoder_log(paraoptlog,islog);
    close all;
    funlog = 'cost_decoder_log';
    disp('running Ellipsoid expansion')                        
    OutE = ELexp(funlog,chi2inv(0.95,ndata-length(PIdx)),MCsamples,bmaxlog',bminlog',10000); % just to have an idea of the ellipoids
    save(['output_files/posterior/posterior' update_date '.mat'],'OutE','-append');
end

loaded = load(['output_files/posterior/posterior' update_date '.mat']); 
OutE = loaded.OutE;
MCsamples = loaded.MCsamples;

%% sample uniformly inside viable regions with Volint to get a distribution of the posterior

OutVArray = cell(1,100);

rng('shuffle');
noise = true;
if any(ismember(parallel.clusterProfiles,'SGE_BSSE'))
    
    startPoolCluster(100);
    parfor i =1:100
        [~,~,~,~,ndata] = cost_decoder(paraopt, paramSpecs, FlMode, PIdx,  noise);
        cost_decoder_log(paraoptlog,islog);
        close all;
        funlog = 'cost_decoder_log';
        fprintf('running uniform sampling inside viable region %d/%d \n',k,ninitpoints)                        
        OutVArray{i} = Volint(funlog,chi2inv(0.95,ndata-length(PIdx)), [MCsamples; OutE.V],bmaxlog',bminlog',1000);
    end
    
else
    [~,~,~,~,ndata] = cost_decoder(paraopt, paramSpecs, FlMode, PIdx,  noise);
    cost_decoder_log(paraoptlistlog(1,:),islog);
    close all;
    for i =1:100        
        funlog = 'cost_decoder_log';
        OutVArray{i} = Volint(funlog,chi2inv(0.95,ndata-length(PIdx)), [MCsamples; OutE.V],bmaxlog',bminlog',1000);
    end
end

if isfield(loaded, 'OutV')
    OutV = loaded.OutV;
else
    OutV.V = [];
    OutV.cost = [];
end

for k = 1:100
    if isfield(OutVArray{k},'V')
        OutV.V = [OutV.V; OutVArray{k}.V];
        OutV.cost = [OutV.cost; OutVArray{k}.cost];
    end
end

%%
save(['output_files/posterior/posterior' update_date '.mat'],'OutV','-append');

end