% © 2021, ETH Zurich
function [varargout] = cost_decoder(varargin)
% cost function for the decoder target behavior, or for estimating parameters from modules.

persistent paramFixedValues IC paramNames paramSpecs FlMode paraestinit PIdx  minstd dataDRalpha dataDRaTc dataReleaseDRalpha noise;

% generate MEX file for the model if it does not already exist
modelname = 'decoder_model';
model = IQMmodel(['models/' modelname '.txtbc']);
generateMEXfile(model,modelname);

%% Interpret input arguments

parainput = varargin{1}; % if nargin =1, only subset of estimated parameter 
                       % values for the PIdx indices
                       % if nargin >1, all estimated parameters from the
                       % paramSpecs table

FlPlot = false; % by default do not plot anything

if nargin > 1 % initialization of persistent data
    
    if ~ismember('SGE_BSSE',parallel.clusterProfiles)            
        FlPlot = true; % authorize plotting outside of an optimization run
%                        if we are not on the cluster.
%        FlPlot = false;
    end   
    
    % load data and keep as persistent variables
    loaded = load('output_files/data/dataDRaTc.mat');     
    dataDRaTc = loaded.dataDRaTc; % aTc time-dependent dose response
    loaded = load('output_files/data/dataDRalpha.mat');   
    dataDRalpha = loaded.dataDRalpha; % alpha factor time-dependent dose response
    loaded = load('output_files/data/dataReleaseDRalpha.mat');   
    dataReleaseDRalpha = loaded.dataReleaseDRalpha; % alpha factor release experiment
    dataset = {dataDRaTc dataDRalpha dataReleaseDRalpha};
    
    % flag for adding noise to simulated values. by default no noise. noise
    % will be set to true when computing the parameter posterior
    noise = false;  
    
    % limit of detection extracted from data used as a minimum standard deviation     
    minstd = find_minstd(dataset); 
    
    % Specifications for estimated parameters.    
    paramSpecs = varargin{2}; 
    
    % selecting which cases are simulated. 
    % cost function for the decoder target behavior , FlMode = 1
    % cost function for estimating parameters from all modules, FlMode =
    % 2:11;
    FlMode     = varargin{3}; 
    
    
    % initialize parameter values: load standard model parameters
    [paramNames, values] = IQMparameters(model);
        
    % set fixed parameter values given in the IQM model
    paramFixedValues = [];
    npara=length(paramNames);
    for kpara=1:npara
        paramFixedValues.(paramNames{kpara}) = values(kpara);
    end
    
    % load initial conditions from the model
    IC = IQMinitialconditions(model); 
    
    % set all estimated or projected parameter values when initializing the function
    % same order as in paramSpecs
    paraestinit = parainput; 
    
    % set indices in paramSpecs for a subset of optimized parameters
    PIdx  = varargin{4};
             
    % optional: add noise to the simulations. used for computing parameter
    % posterior to make the acception rejection algorithm work correctly
    if nargin > 4        
        noise = varargin{5};
    end
    
end

%% Make estimated parameter vector:
% Start from persistent already estimated values and change parameter values for a subset of indices 

paramValues    = paramFixedValues; % all parameter values including fixed and estimated

EstParamNames = paramSpecs.names;
nparaest = length(EstParamNames); % total number of potentially estimated or projected parameters

if (length(parainput) ~= nparaest && nargin > 1)
    error('During initialization the input paraest should have the same dim as paramSpecs.names');
end


if (length(parainput) ~= length(PIdx) && nargin < 2)
    error('After initialization the input paraest should have the same dim as PIdx');
    
end

% create estimated parameter vector

paraest = paraestinit;

if nargin < 2 
    % only after initialization change only parameters of indices PIdx
    for kidx = 1:length(PIdx)
        paraest(PIdx(kidx)) = parainput(kidx);
    end    
end


% update all parameter values structure with estimated params
for kparaest = 1:nparaest
    if ~isfield(paramValues,EstParamNames{kparaest})            
        error('parameter in paramSpecs not defined in the model')
    else
        paramValues.(EstParamNames{kparaest}) = paraest(kparaest);                              
    end
end

%% Run simulations

TFlMode = FlMode;
costvector    = []; % cost values for all modules
expnames   = []; % names of experiments (design objective or modules)
ndata   = 0; % # data points
SData   = []; % simulation data

    
while ~isempty(TFlMode)
    
    TFl = TFlMode(1);
    switch TFl       

        %% case 1: design objective experiment, measure steady state (SS) for short and long induction signal (20,30 min vs 180 min)
        case {1} 
            
            expname = 'goal';
            paramValuesForThisCase = paramValues;
            paramValuesForThisCase.ind_time = 400; % 400min to reach an initial SS
            tmax = paramValuesForThisCase.ind_time + 7*60;
            tspan = 0:1:tmax;
            SData.(expname).ind_time =  paramValuesForThisCase.ind_time;
            SData.(expname).tspan = tspan';
            ind_duration = [0 10 20 30 40 80 120 180 360]; % duration of the pulse.
            % several durations to understand what happens but we use only
            % 0, 20, 30 and 180 for the cost
            
            map = flipud(parula(length(ind_duration)+1));
            obslist = { 'alpha'  'FP' 'TF1' 'TF2on'};  % choose which observables to save and plot
            map = map(2:end,:);
            
            % simulate design objective and plot

            for k = 1:length(ind_duration)
                
                paramValuesForThisCase.ind_duration = ind_duration(k);
                para = fconvpara(paramValuesForThisCase); % convert parameters from structure to vector 
                output = IQMPsimulate(modelname, tspan , IC, paramNames, para); 

                for kobs = 1:length(obslist)
                    obs = obslist{kobs};
                    duration_name = [obs sprintf('_%d',ind_duration(k))];
                    [SData.(expname).(duration_name)] = zeros(length(tspan),1);
                    
                    % observables can be states or variables
                    if sum(ismember(output.states, obs)) == 1
                        index = ismember(output.states, obs);
                        SData.(expname).(duration_name) = output.statevalues(:,index);
                    else
                        index = ismember(output.variables, obs);
                        SData.(expname).(duration_name) = output.variablevalues(:,index);
                    end 
                    
                    if FlPlot
                        subplot(length(obslist),1,kobs)
                        % in plots time 0 is when we add alpha. we put time
                        % in hours
                        plot((tspan - paramValuesForThisCase.ind_time)/60, SData.(expname).(duration_name),'Color', map(k,:));
                        hold on;
                        title(obslist{kobs})
                        set(gca,'FontSize',12)
                        xlim([-1 7])
                    end    
                end  

            end

            if FlPlot
                    str = sprintf('%d min induction_',ind_duration(1:end));
                    str = strsplit(str,'_');
                    legend(str(1:end-1));
                    xlabel('time (h)')
                    ylabel('concentration (apu)')    
            end
            
            % calculate cost

            tind = find(tspan == paramValuesForThisCase.ind_time);
            t6h = find(tspan == paramValuesForThisCase.ind_time + 360);

            d0 = 0;
            d30 = 30;
            d20 = 20;
            d2 = 180;

            cost_basal = (SData.(expname).(sprintf('FP_%d',d2))(tind)/100);
            cost_detect_30 = 100/SData.(expname).(sprintf('FP_%d',d30))(t6h); % We want something at least on the order of Act1 value which is kact1/rho = 1/0.0077 ~ 130
            cost_detect_20 = 100/SData.(expname).(sprintf('FP_%d',d20))(t6h);
            cost_detect = max(cost_detect_30, cost_detect_20); % we want a range to avoid fine tuning around one value
            cost_30vs0 = 20*max(SData.(expname).(sprintf('FP_%d',d0)))/SData.(expname).(sprintf('FP_%d',d30))(t6h);
            cost_180vs30 = 20*max(SData.(expname).(sprintf('FP_%d',d2)))/SData.(expname).(sprintf('FP_%d',d30))(t6h);

            % display cost components together with plotting
            if FlPlot
                disp(table(cost_basal,cost_30vs0,cost_180vs30,cost_detect))
            end
            
            % add components to final cost (for design objective only one dimension for expnames and costvector)
            
            expnames{end+1} = expname;
            costvector(end+1) =  max([cost_basal^2 cost_30vs0^2 cost_180vs30^2 cost_detect^2]);


    %% responses to alpha factor

        %% yCL103 = FRY2905 = fus1tet_Citrine
        % all strains yCL also have a FRY ID which was used to save the
        % data.

        case{2}

            icase = 2;
            strain_name = 'FRY2905'; 
            yCL_name = 'yCL103';
            paramValuesForThisCase = paramValues;
            
            % project parts not involved in this module
            paramValuesForThisCase.kd1 = 1; % no post-translational interaction on TF1
            paramValuesForThisCase.kd2 = 1;
            paramValuesForThisCase.k2aTF1 = 0; % fus1tet_LexA projected 
            paramValuesForThisCase.k12TF1 = 0; % lexAtet_LexA projected           
            paramValuesForThisCase.k12TF2 = 0; % lexAtet_TetR projected
            paramValuesForThisCase.k2aTF2 = 0; % lexAtet_TetR projected   
            paramValuesForThisCase.k12FP = 0; % lexatet_Citrine projected

            % fus1tet_Citrine not projected
            % set Km2 value equal to the only one that will be estimated:
            % Km21FP. No need to do it for Kma since Kma2FP is already 
            % the one we decided to estimate to represent all Kma.          
            
            paramValuesForThisCase.Km2aFP = paramValuesForThisCase.Km21FP;
            
            % simulate response to alpha factor doses
            expname = [yCL_name 'alphaDR'];
            paramValuesForThisCase.tc = 0;   % no aTc             
            SData  = simulate_DR_alpha(dataDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         
            
            % simulate alpha factor release
            expname = [yCL_name 'alphaRelease'];
            paramValuesForThisCase.tc = 0;   % no aTc             
            SData  = simulate_alpha_release(dataReleaseDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);            

        %% yCL104 = FRY2906 = fus1tet_LexA + lexatet_Citrine                                
            case{3}
            icase = 3;
            
            paramValuesForThisCase = paramValues;

            strain_name = 'FRY2906';
            yCL_name = 'yCL104';
             
            
            % fus1tet_LexA not projected
            % set k2a value and Km2 values equal to the only one that will be estimated:
            % k2aFP, Km21FP and Kma2FP.     
            paramValuesForThisCase.k2aTF1 = paramValuesForThisCase.k2aFP; % fus1tet-LexA
            paramValuesForThisCase.Km2aTF1 = paramValuesForThisCase.Km21FP;   
            paramValuesForThisCase.Kma2TF1 = paramValuesForThisCase.Kma2FP; 
            
            % lexAtetCitrine not projected
            % No need to do change k12FP or Km21FP, Km12FP values since these are already 
            % the ones we decided to estimate fro k12, Km21, Km12.  
            
            % project values for unused constructs (be careful to project
            % after setting the non projected values otherwise it could
            % change the reference values accidentally)
            
            paramValuesForThisCase.kd1 = 1; % no post-translational interaction on TF1            
            paramValuesForThisCase.kd2 = 1; % no post-translational interaction on TF2             
            paramValuesForThisCase.k12TF1 = 0;  % lexAtet_LexA projected   
            paramValuesForThisCase.k12TF2 = 0; % lexAtet_TetR projected
            paramValuesForThisCase.k2aTF2 = 0;  % fus1tet_TetR projected      
            paramValuesForThisCase.k2aFP = 0; % lexAtet_Citrine projected   
            
            % simulate response to alpha factor doses
            expname = [yCL_name 'alphaDR'];
            paramValuesForThisCase.tc = 0; % no aTc            
            SData  = simulate_DR_alpha(dataDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         
            
            % simulate alpha factor release
            expname = [yCL_name 'alphaRelease'];
            paramValuesForThisCase.tc = 0; % no aTc    
            SData  = simulate_alpha_release(dataReleaseDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);      
            
        %% yCL105 = FRY2907 = fus1tet_Citrine + Pact1_TetR            
        case{4}
            
            icase = 4;
            strain_name = 'FRY2907';
            yCL_name = 'yCL105';
            paramValuesForThisCase= paramValues;
            
            
            
            %to have a pAct1 TetR in the system we set k2aTF2, Kma2TF2 and Km2aTF2
            %such that the corresponding term in the equations is equal to 1.
            
            paramValuesForThisCase.k12TF2 = 1/paramValuesForThisCase.kL1; 
            paramValuesForThisCase.Km12TF2 = 1e10;
            paramValuesForThisCase.Km21TF2 = 1e10;
            
            % We add the induced part of Pact1_TetR by using a non
            % repressible k2a promoter.
            paramValuesForThisCase.k2aTF2 = paramValuesForThisCase.kact_induced; 
            paramValuesForThisCase.Km2aTF2 = 1e10;
            
            % fus1tet Citrine is not projected
            % we set the Km2 value equal to the reference Km2
            % other parameters are already the reference.
            paramValuesForThisCase.Km2aFP = paramValuesForThisCase.Km21FP; 
            
            % projected values
            paramValuesForThisCase.kd1 = 1;
            paramValuesForThisCase.kd2 = 1;
            paramValuesForThisCase.k2aTF1 = 0;
            paramValuesForThisCase.k12TF1 = 0;
            paramValuesForThisCase.k12FP = 0; 
            
            % simulate response to alpha factor doses
            expname = [yCL_name 'alphaDR'];
            paramValuesForThisCase.tc = 0;     % no aTc           
            SData  = simulate_DR_alpha(dataDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         

        %% 	yCL106 = FRY2908 = pAct1Citrine

        case{5}
            icase = 5;
            strain_name = 'FRY2908';
            yCL_name = 'yCL106';
            paramValuesForThisCase= paramValues;
                      

            % We add the induced part of Pact1_Citrine by using a non
            % repressible k2a promoter.
            paramValuesForThisCase.k2aFP = paramValuesForThisCase.kact_induced; 
            paramValuesForThisCase.Km2aFP = 1e10;
            
            % We add The constitutive part of Pact1_Citrine leading to a term = 1.
            paramValuesForThisCase.k12FP = 1/paramValuesForThisCase.kL1;             
            P.Km21FP = 1e10;
            P.Km12FP = 1e10;

            expname = [yCL_name 'alphaDR'];
            paramValuesForThisCase.tc = 0; % no aTc               
            
            % project unused parts
            paramValuesForThisCase.kd1 = 1;
            paramValuesForThisCase.kd2 = 1;
            paramValuesForThisCase.k2aTF1 = 0;
            paramValuesForThisCase.k12TF1 = 0;
            paramValuesForThisCase.k12TF2 = 0;
            paramValuesForThisCase.k2aTF2 = 0;  
            
            % simulate response to alpha factor doses
            SData  = simulate_DR_alpha(dataDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);   

        %% yCL113 = FRY2934 = pAct1_taggedLexA + lexatet_Citrine

        case{6}
            
            icase = 6;

            strain_name = 'FRY2934'; 
            yCL_name = 'yCL113';
            paramValuesForThisCase = paramValues;
            

            % not projecting kd1 to have a tagged LexA
            paramValuesForThisCase.d1 = paramValuesForThisCase.d1tag;
            
            % not projecting k12FP: lexAtet_Citrine
            
            % We add the induced part of Pact1_LexA by using a non
            % repressible k2a promoter.
            paramValuesForThisCase.k2aTF1 = paramValuesForThisCase.kact_induced; 
            paramValuesForThisCase.Km2aTF1 = 1e10;

            % We add The constitutive part of Pact1_LexA leading to a term = 1.
            paramValuesForThisCase.k12TF1 = 1/paramValuesForThisCase.kL1; 
            P.Km21TF1 = 1e10;
            P.Km12TF1 = 1e10;           
            
            % project unused parts
            paramValuesForThisCase.k12TF2 = 0; 
            paramValuesForThisCase.k2aTF2 = 0;     
            paramValuesForThisCase.k2aFP = 0; 
            
            % simulate response to alpha factor doses
            expname = [yCL_name 'alphaDR'];
            paramValuesForThisCase.tc = 0;                
            SData  = simulate_DR_alpha(dataDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise); 
            
            % simulate alpha factor release
            expname = [yCL_name 'alphaRelease'];
            paramValuesForThisCase.tc = 0;                
            SData  = simulate_alpha_release(dataReleaseDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise); 

        %% yCL107 = FRY2909 = fus1tet_LexA + lexatet_LexA + lexatet_Citrine
        % for positive feedback dynamics cannot be fitted with a
        % deterministic model
        % only the 18h steady state is taken into account
        
        case{7}
            
            icase = 7;

            strain_name = 'FRY2909';
            yCL_name = 'yCL107';
            paramValuesForThisCase= paramValues;
            
            
            % fus1tet_LexA not projected
            paramValuesForThisCase.k2aTF1 = paramValuesForThisCase.k2aFP; 
            paramValuesForThisCase.Km2aTF1 = paramValuesForThisCase.Km21FP;   
            paramValuesForThisCase.Kma2TF1 = paramValuesForThisCase.Kma2FP;   

            % lexAtet_LexA not projected
            paramValuesForThisCase.k12TF1 = paramValuesForThisCase.k12FP; 
            paramValuesForThisCase.Km12TF1 = paramValuesForThisCase.Km12FP;
            paramValuesForThisCase.Km21TF1 = paramValuesForThisCase.Km21FP;
            
            % lexAtet_Citrine not projected but parameters are already the
            % reference
            
            % project parameters
            paramValuesForThisCase.kd1 = 1;
            paramValuesForThisCase.kd2 = 1;
            paramValuesForThisCase.k12TF2 = 0; 
            paramValuesForThisCase.k2aTF2 = 0;     
            paramValuesForThisCase.k2aFP = 0; 
            
            % simulate response to alpha factor doses
            expname = [yCL_name 'alphaDR'];
            paramValuesForThisCase.tc = 0;                
            SData  = simulate_DR_alpha(dataDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         

            % simulate alpha factor release (only the initial point = SS
            % value of alpha factor dose response at 18h)
            dataRelease_time0 = dataReleaseDRalpha;
            dataRelease_time0.timepoints = 0;
            expname = [yCL_name 'alphaRelease'];
            paramValuesForThisCase.tc = 0;                
            SData  = simulate_alpha_release(dataRelease_time0, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         

        %% yCL109 = FRY2911 = fus1mut_TetR + pAct1_LexA + lexatet_Citrine
        case{8}
            
            icase = 8;

            strain_name = 'FRY2911';
            yCL_name = 'yCL109';
            paramValuesForThisCase = paramValues;                        
            
            
            % We add the induced part of Pact1_LexA by using a non
            % repressible k2a promoter.
            paramValuesForThisCase.k2aTF1 = paramValuesForThisCase.kact_induced; % pAct1 LexA induced
            paramValuesForThisCase.Km2aTF1 = 1e10;      
            
            % We add The constitutive part of Pact1_LexA leading to a term = 1.
            paramValuesForThisCase.k12TF1 = 1/paramValuesForThisCase.kL1; % pAct1 LexA non induced
            P.Km21TF1 = 1e10;
            P.Km12TF1 = 1e10; 

            % fus1mut_TetR not projected
            % no self repression
            paramValuesForThisCase.k2aTF2 = paramValuesForThisCase.k2aFP; 
            paramValuesForThisCase.Kma2TF2 = paramValuesForThisCase.Kma2FP; 
            paramValuesForThisCase.Km2aTF2 = 1e10;
            
            % lexAtet_Citrine not projected (already reference parameters)
            
            % project parameters for unused parts
            paramValuesForThisCase.kd1 = 1; 
            paramValuesForThisCase.kd2 = 1;
            paramValuesForThisCase.k12TF2 = 0; 
            paramValuesForThisCase.k2aFP = 0; 
            
            % simulate response to alpha addition
            expname = [yCL_name 'alphaDR'];
            paramValuesForThisCase.tc = 0;                
            SData  = simulate_DR_alpha(dataDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         
            
            % simulate response to alpha release
            expname = [yCL_name 'alphaRelease'];
            paramValuesForThisCase.tc = 0;                
            SData  = simulate_alpha_release(dataReleaseDRalpha, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         


    %% responses to aTc
    
        %% yCL105 = FRY2907 = fus1tetCitrine_Pact1TetR
        case{9}

            strain_name = 'FRY2907';
            yCL_name = 'yCL105';
            icase = 9;
            
            paramValuesForThisCase = paramValues;                
            

            % pAct1_TetR induced part
            paramValuesForThisCase.k2aTF2 = paramValuesForThisCase.kact_induced; 
            paramValuesForThisCase.Km2aTF2 = 1e10;          

            % pAct1_TetR constitutive part
            paramValuesForThisCase.k12TF2 = 1/paramValuesForThisCase.kL1; 
            P.Km21TF2 = 1e10;
            P.Km12TF2 = 1e10; 

            % fus1tet Citrine not projected
            paramValuesForThisCase.Km2aFP = paramValuesForThisCase.Km21FP;

            % project unused parts
            paramValuesForThisCase.kd1 = 1;
            paramValuesForThisCase.kd2 = 1;
            paramValuesForThisCase.k2aTF1 = 0;
            paramValuesForThisCase.k12TF1 = 0;
            paramValuesForThisCase.k12FP = 0; 
            
            % simulate response to aTc for alpha = 0
            expname = [yCL_name 'aTcDRalpha0'];
            paramValuesForThisCase.alphamax = 0;                
            SData  = simulate_DR_aTc(dataDRaTc, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         

            % simulate response to aTc for alpha = 1000 nM
            expname = [yCL_name 'aTcDRalpha1000'];
            paramValuesForThisCase.alphamax = 1000;                
            SData  = simulate_DR_aTc(dataDRaTc, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise); 




        %% yCL109 = FRY2911 = fus1mut_TetR + pAct1_LexA + lexatet_Citrine
        case{10}
                        
            strain_name = 'FRY2911';
            yCL_name = 'yCL109';
            icase = 10;                                    
            paramValuesForThisCase = paramValues;                        
            
            
            % We add the induced part of Pact1_LexA by using a non
            % repressible k2a promoter.
            paramValuesForThisCase.k2aTF1 = paramValuesForThisCase.kact_induced;
            paramValuesForThisCase.Km2aTF1 = 1e10;      
            
            % We add the constitutive part of Pact1_LexA leading to a term = 1.
            paramValuesForThisCase.k12TF1 = 1/paramValuesForThisCase.kL1; 
            P.Km21TF1 = 1e10;
            P.Km12TF1 = 1e10; 

            % fus1mut_TetR not projected
            % no self repression
            paramValuesForThisCase.k2aTF2 = paramValuesForThisCase.k2aFP; 
            paramValuesForThisCase.Kma2TF2 = paramValuesForThisCase.Kma2FP; 
            paramValuesForThisCase.Km2aTF2 = 1e10;
            
            % lexAtet_Citrine not projected (already reference parameters)

            % project parameters for unused parts
            paramValuesForThisCase.kd1 = 1; 
            paramValuesForThisCase.kd2 = 1;
            paramValuesForThisCase.k12TF2 = 0; 
            paramValuesForThisCase.k2aFP = 0; 
            
            expname = [yCL_name 'aTcDRalpha0'];
            paramValuesForThisCase.alphamax = 0;                
            SData  = simulate_DR_aTc(dataDRaTc, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         

            expname = [yCL_name 'aTcDRalpha1000'];
            paramValuesForThisCase.alphamax = 1000;                
            SData  = simulate_DR_aTc(dataDRaTc, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise); 

        %% yCL110 = FRY2931  = pAct1_TetR + pAct1_LexA + lexatet_Citrine            
        
        case{11}
            
            strain_name = 'FRY2931';
            yCL_name = 'yCL110';
            icase = 11;
            paramValuesForThisCase= paramValues;
            
            
            
            % pAct1 LexA induced part
            paramValuesForThisCase.k2aTF1 = paramValuesForThisCase.kact_induced; 
            paramValuesForThisCase.Km2aTF1 = 1e10;
            
            % pAct1 LexA constitutive part
            paramValuesForThisCase.k12TF1 = 1/paramValuesForThisCase.kL1; 
            P.Km21TF1 = 1e10;
            P.Km12TF1 = 1e10; 


            % pAct1_TetR constitutive part
            paramValuesForThisCase.k2aTF2 = paramValuesForThisCase.kact_induced;  % pAct1 TetR
            paramValuesForThisCase.Km2aTF2 = 1e10;
            
            % pAct1_TetR induced part
            paramValuesForThisCase.k12TF2 = 1/paramValuesForThisCase.kL1;  % pAct1 TetR
            paramValuesForThisCase.Km12TF2 = 1e10;
            paramValuesForThisCase.Km21TF2 = 1e10;
            

                        
            % lexAtet_Citrine not projected (already reference parameters)

            % project unused parts
            paramValuesForThisCase.kd1 = 1; % no tagged LexA
            paramValuesForThisCase.kd2 = 1; % no tagged TetR
            paramValuesForThisCase.k2aFP = 0; 

            expname = [yCL_name 'aTcDRalpha0'];
            paramValuesForThisCase.alphamax = 0;                
            SData  = simulate_DR_aTc(dataDRaTc, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         

            expname = [yCL_name 'aTcDRalpha1000'];
            paramValuesForThisCase.alphamax = 1000;                
            SData  = simulate_DR_aTc(dataDRaTc, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise); 

%% yCL110 = FRY2931  = pAct1_TetR + pAct1_LexA + lexatet_Citrine            
        
        case{12}
            
            strain_name = 'FRY2931';
            yCL_name = 'yCL110';
            icase = 12;
            paramValuesForThisCase= paramValues;
            
            
            
            % pAct1 LexA induced part
            paramValuesForThisCase.k2aTF1 = paramValuesForThisCase.kact_induced; 
            paramValuesForThisCase.Km2aTF1 = 1e10;
            
            % pAct1 LexA constitutive part
            paramValuesForThisCase.k12TF1 = 1/paramValuesForThisCase.kL1; 
            P.Km21TF1 = 1e10;
            P.Km12TF1 = 1e10; 


            % pAct1_TetR constitutive part
            paramValuesForThisCase.k2aTF2 = paramValuesForThisCase.kact_induced;  % pAct1 TetR
            paramValuesForThisCase.Km2aTF2 = 1e10;
            
            % pAct1_TetR induced part
            paramValuesForThisCase.k12TF2 = 1/paramValuesForThisCase.kL1;  % pAct1 TetR
            paramValuesForThisCase.Km12TF2 = 1e10;
            paramValuesForThisCase.Km21TF2 = 1e10;
            

                        
            % lexAtet_Citrine not projected (already reference parameters)

            % project unused parts
            paramValuesForThisCase.kd1 = 1; % no tagged LexA
            paramValuesForThisCase.kd2 = 1; % no tagged TetR
            paramValuesForThisCase.k2aFP = 0; 

            expname = [yCL_name 'aTcDRalpha0'];
            paramValuesForThisCase.alphamax = 0;
            dataDRaTc.alpha_array = dataDRaTc.alpha_array(1);
            SData  = simulate_DR_aTc(dataDRaTc, SData, expname, paramValuesForThisCase, modelname, IC, paramNames, FlPlot, icase, strain_name, minstd);
            [expnames, costvector, ndata] = update_cost(SData, expname, expnames, costvector, ndata, noise);         

    end

        TFlMode = setdiff(TFlMode,TFl);

end

% calculate cost, the sum of costvector components that are already sum(residuals^2)

cost = sum(costvector);


if nargin > 2
disp(['cost' sprintf('\t\t%e',cost)])
end

varargout{1} = cost;
varargout{2} = costvector;
varargout{3} = expnames;
varargout{4} = SData;
varargout{5} = ndata;


return