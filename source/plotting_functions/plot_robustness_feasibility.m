% © 2021, ETH Zurich
function [] = plot_robustness_feasibility(RankedCircuits, update)

% define marker types for different motifs

nTopo = size(RankedCircuits,1);
MarkerType = cell(nTopo,1);
MarkerColor = cell(nTopo,1);
idstr = cell(nTopo,1);
for k = 1:nTopo
    if RankedCircuits(k).IFF == 1 && RankedCircuits(k).NF == 1 && RankedCircuits(k).PF == 1
       MarkerColor{k} = 'r';
    elseif RankedCircuits(k).IFF == 1 && RankedCircuits(k).PF == 1
         MarkerColor{k} = 'g';
    elseif RankedCircuits(k).IFF == 1 && RankedCircuits(k).NF == 1
         MarkerColor{k} = 'b';
    elseif RankedCircuits(k).IFF == 1
         MarkerColor{k} = 'w';
    end
    MarkerType{k} = 'o';    
    idstr{k} = num2str(RankedCircuits(k).publication_id);
end

fieldname = [update '_feasibility'];
figure();
for k = 1:nTopo
    plot([RankedCircuits(k).logrobustness],[RankedCircuits(k).(fieldname)],'.','MarkerSize',15,'MarkerFaceColor',MarkerColor{k},'Color','k','Marker',MarkerType{k},'LineWidth',2);
    text([RankedCircuits(k).logrobustness],[RankedCircuits(k).(fieldname)],idstr{k},'HorizontalAlignment','center','VerticalAlignment','middle');
    hold on;
end
xlabel('Log_{10}(robustness)')
ylabel([update ' feasibility'])

end