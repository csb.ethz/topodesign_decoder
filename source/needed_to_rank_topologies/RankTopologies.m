% © 2021, ETH Zurich
function [] = RankTopologies(iMod, topofilter_run)

    % computes metrics for topologies that came out of the
    % topofilter run:
    % robustness
    % ideal feasibility
    % updated feasibility if a parameter posterior is available
    % computes the high feasibility regions
    % generates a folder 'output_files/ranked_topologies' with a structure
    % OutV for each topology.


    %% define path and load topofilter settings 
    
    loaded = load(['output_files/TopoFilter_output_samples/' topofilter_run '/' topofilter_run '.mat']);
    settings = loaded.settings;
    settings.parallelize = 0; 
    settings.cleanupMexFiles = 0;
    
    folderPath = fullfile(['output_files/TopoFilter_output_samples/' topofilter_run '/']);
    fileNamePattern = '*_viablePoints_*.mat';
    fileNames = dir(fullfile(folderPath, fileNamePattern));
    fileNames = {fileNames.name};

    assert(~isempty(fileNames),'No files found. Are you in the correct folder? If yes, check the path and the file name pattern?')

    rmDuplicateProjections=true; % Out of duplicates, take viable points with the largest sample

    %% load saved viable points which are the output of TopoFilter and remove potential duplicates
    
    nMod = numel(fileNames);
    for iPar=nMod:-1:1
        fn = fileNames{iPar};
        loaded = load(fullfile(folderPath,fn));
        viablePointsArray(iPar) = loaded.viablePoints;
        % for duplicates removal
        projectionArray(iPar,:) = loaded.viablePoints.projection';
    end

    % Remove duplicates
    if rmDuplicateProjections        
        [~, uniqueProjectionRowIdxs] = unique(projectionArray,'rows','stable');
        repeatsProjectionRowIdxs = setdiff(1:nMod, uniqueProjectionRowIdxs);
        repeatsProjectionArray = projectionArray(repeatsProjectionRowIdxs,:);
        nMod = numel(uniqueProjectionRowIdxs);
        for iPar=nMod:-1:1
            projectionIdx = uniqueProjectionRowIdxs(iPar);
            projection = projectionArray(projectionIdx,:);
            repeatsIdxs = find(ismember(repeatsProjectionArray, projection, 'rows'));
            maxSampSize = numel(viablePointsArray(projectionIdx).cost);
            for iRep=1:numel(repeatsIdxs)
                repeatIdx = repeatsProjectionRowIdxs(repeatsIdxs(iRep));
                repeatSampSize = numel(viablePointsArray(repeatIdx).cost);
                if repeatSampSize > maxSampSize
                    maxSampSize = repeatSampSize;
                    projectionIdx = repeatIdx;
                end
            end
            uniqueProjectionRowIdxs(iPar) = projectionIdx;
        end

        viablePointsArray = viablePointsArray(uniqueProjectionRowIdxs);
    end


    %% prepare topofilter models and data

    model = loadModel(settings);
    experiments = loadExperiments(settings);   
    nExp = numel(experiments);
    mexNames = cell(nExp,1);
    mexCleanups = cell(nExp,1);
    for iPar = 1:nExp
        experiment = experiments{iPar};
        expmodel = IQMmergemodexp(model,experiment);            
        [mexNames{iPar}, mexCleanups{iPar}] = mexModel(expmodel,settings,iPar);
    end

    expData = loadExpData(settings);
    simopts = [];
    simopts.solNrTimepoints = settings.odeIntegratorSolNrTimepoints; % number of time points
    threshold = expData.threshold;
%     expData.maxT = 820;

    newfoldername = ['output_files/ranked_topologies/' topofilter_run '/']; %prepare folder to save ranked topologies for this run.
    if ~exist(newfoldername)
        mkdir(newfoldername);
    end

    paramSpecs = loadParamSpecs(settings);
    paramSpecsNames = paramSpecs.names;
    paramSpecsbmax = paramSpecs.bmax;
    paramSpecsbmin = paramSpecs.bmin;
    
    nsamp = settings.nfeval/10; % number of samples for ranking    

    fprintf('RankProjections, ranking topology #%d/%.2f \n',iMod,nMod);            
        
%%  compute robustness: sample uniformly inside viable regions with Hyperspace
    
    if ~exist([newfoldername sprintf('OutV_%d.mat',iMod)],'file')
        
        %%  prepare points with the values of projected parameters        

        points = viablePointsArray(iMod);
        projectionStr = strrep(mat2str(int8(points.projection(:))),';','');
        projectionStr = projectionStr(2:(end-1));
        fprintf('RankProjections, prepare points #%d/%d, projection = %s .. \n',iMod,nMod,projectionStr);

        [npoints,nparvar] = size(points.rowmat);   

        if ~isfield(points, 'projected')
            projected.names = {};
            projected.values = [];
        else
            projected = points.projected;
        end
        if ~isempty(projected.islog)
            for i = 1:length(projected.values)
                if projected.values(i) == 0 && projected.islog(i) == 1
                    projected.islog(i) = 0;
                end
            end
            projected.values(projected.islog) = 10.^projected.values(projected.islog);
        end
        paramNames = vertcat(points.colnames, projected.names);
        npartot = numel(paramNames);
        fixedParamValuesTail = projected.values;
        if ~isfield(points, 'islog') || isempty(points.islog)
            islog = false(1,nparvar);
        else
            islog = points.islog;
        end        

        %% reduce points to same Km subspace 
        % to sample only one Kmx value for all Kmx2y and only one Km2 value
        % for all Km2xy.

        circuitnames = points.colnames;

        [ ~, ~,  SubspaceIdx ] = simulate_circuit_sameKm(mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames); 

        circuitnamesInSubspace = circuitnames(SubspaceIdx);

        points.rowmat = points.rowmat(:,SubspaceIdx);   

        %% Get bmin and bmax (needed for Volint and for robustness estimation)

        % mapping from the original parameters order to the order with first
        % variable and then fixed parameters

        paramNamesIdx = zeros(1,npartot);
        for iPar=1:npartot
            pName = paramNames{iPar};
            paramNamesIdx(iPar) = find(strcmp(pName,paramSpecsNames));
        end
        bmax = paramSpecsbmax(paramNamesIdx)';
        bmin = paramSpecsbmin(paramNamesIdx)';
        % drop bound for fixed parameters
        bmax = bmax(1:nparvar);
        bmin = bmin(1:nparvar);
        % set log-scale for bounds
        bmax(islog) = log10(bmax(islog));
        bmin(islog) = log10(bmin(islog));
        %drop bounds for subspace with same Km
        bmax = bmax(SubspaceIdx);
        bmin = bmin(SubspaceIdx); 


        % compute which points are still viable when Kms are equal        

        cost = 2*ones(npoints,1);
        for k = 1:npoints
            cost(k) = simulate_circuit_sameKm(points.rowmat(k,:));
        end

        stillviable = (cost<1);
        variableParamValuesHeadRowmat = points.rowmat(stillviable,:);

        % sample uniformly in the same Km subspace        

        dim = size(variableParamValuesHeadRowmat,2);
        npointsInSubspace =  size(variableParamValuesHeadRowmat,1);


        if  npointsInSubspace == 0
            fprintf('no point was found in same Km subspace for projection = %d, %s .. \n ', iMod,projectionStr);
            % We drop these topologies. We still save an 'OutV' file for
            % record.

        elseif  npointsInSubspace < 10*(dim+1) % resample ellipsoids if we do not have enough samples to directly run Volint

            OutMV =[];
            for k = 1:size(variableParamValuesHeadRowmat,1)
                OutM = MCexp('simulate_circuit_sameKm', threshold, variableParamValuesHeadRowmat(k,:), bmax, bmin, 10*floor(nsamp/npointsInSubspace));
                OutMV = [OutMV; OutM.V];
            end
            OutE = ELexp('simulate_circuit_sameKm', threshold, OutMV, bmax, bmin, nsamp);        
            fprintf('RankProjections, Volint #%d/%d, projection = %s .. \n',iMod,nMod,projectionStr);
            OutV = Volint('simulate_circuit_sameKm', threshold, OutE.V, bmax, bmin, nsamp);  

        else 
            fprintf('RankProjections, Volint #%d/%d, projection = %s .. \n',iMod,nMod,projectionStr);
            OutV = Volint('simulate_circuit_sameKm', threshold, variableParamValuesHeadRowmat, bmax, bmin, nsamp);        
        end     

        OutV.id = iMod;
        OutV.circuitnames = circuitnames; 
        OutV.circuitnamesInSubspace = circuitnamesInSubspace;
        OutV.robustness = OutV.vol/prod(bmax-bmin);
        OutV.robustness_err = OutV.err/prod(bmax-bmin);
        
        else       
    %%  to rank topologies again without recomputing OutV

        loaded = load([newfoldername sprintf('OutV_%d.mat',iMod)]);
        OutV = loaded.OutV;
        
    end
        
    %% assign motifs
    
    circuitnames = OutV.circuitnames;
    OutV.IFF = 0;
    if ismember('Km2aTF1',circuitnames) && ismember('Kma2TF2',circuitnames)
        OutV.IFF = 1;
    end
    if ismember('k2aTF1',circuitnames) && ismember('kd1',circuitnames)
        OutV.IFF = 1;
    end
    if ismember('k2aTF1',circuitnames) && ismember('k2aTF2',circuitnames) && ismember('Km21TF1',circuitnames)
        OutV.IFF = 1;
    end    

    if ismember('k2aTF1',circuitnames) && ismember('k2aTF2',circuitnames) && ismember('Km21FP',circuitnames)
        OutV.IFF = 1;
    end    

    if ismember('Km12TF1',circuitnames)
         OutV.PF = 1;
    else
         OutV.PF = 0;
    end

    OutV.NF = 0;

    if ismember('Km2aTF1',circuitnames)&& ismember('k12TF2',circuitnames)
        OutV.NF = 1;
    end

    if ismember('Km2aTF2',circuitnames) || ismember('Km21TF2',circuitnames)
        OutV.NF = 1;
    end     

        
    % function to allow save in a parallel loop    
    iSaveOutV([newfoldername sprintf('OutV_%d',iMod)],OutV); %save OutV
    
    
    
    %% compute ideal feasibility  
    
    % for all feasibility analyses:
    
    projectedindices = ~ismember(paramSpecsNames,circuitnames);
    fixedParamValuesTail = paramSpecs.projections(projectedindices);
    paramNames = vertcat(circuitnames, paramSpecsNames(projectedindices));
    islog = paramSpecs.islog(ismember(paramSpecsNames,circuitnames));
    

    if isfield(OutV, 'V' ) && ~isfield(OutV, 'ideal_feasibility')
        % we check if the circuit has not been dropped because not functional when reducing to the same Km subspace.
        % we check if the ideal feasibility has not already been computed

        % prepare gaussian for ideal feasibility analysis
                

        [ ~, ~,  SubspaceIdx] = simulate_circuit_sameKm(mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);  
        ndimSubspace = length(SubspaceIdx);
        c = median(OutV.V,1);
        ideal_gaussian = [];
        ideal_gaussian.V = mvnrnd(c,diag((log10(2)/4).^2*ones(1,ndimSubspace)),nsamp); 
        % we want an ideally precise standard deviation std:
        % such that 4*std = log10(2) and we give variance = std^2


        % perform optimization

        fprintf('RankProjections, ideal feasibility #%d/%.2f \n',iMod,nMod);            
        find_index_function = @find_tuning_idx_all;   %    optimize feasibility by tuning all parameters            
        [OutV.ideal_feasibility, OutV.ideal_tuning_factors,  OutV.ideal_idx_viable] = tune_feasibility(OutV,...
            ideal_gaussian, find_index_function, paramSpecs, mexNames, expData,...
            paramNames, islog, fixedParamValuesTail, simopts, circuitnames);                     
        iSaveOutV([newfoldername sprintf('OutV_%d',iMod)],OutV); 
        % save OutV again
        % we save the optimal tuning factors and the set of indices of the
        % gaussian that were viable.
        
    end
    
    %% Compute updated feasibility with posterior and explore high feasibility region for selected circuits
    % choose the posterior updated with the last experiments (give the
    % date)
    update_date = '191003'; % leave empty if does not exist yet
    filename = ['output_files\posterior\posterior' update_date '.mat'];
    
    if exist(filename)  && isfield(OutV, 'V' ) && ~ismember('kd2',circuitnames) 
        
    % we update feasibility only if the posterior already exists
    % we exclude circuits with kd2 because it was not part of the posterior estimation
    
    
        %%  prepare posterior by mapping it to the circuit space
        
        loaded = load(filename);
        posterior = loaded.OutV;
        tc_idx = find(ismember(paramSpecs.names,'tc'));
        
        
        PIdxPosterior = [loaded.PIdx; tc_idx]; %adding tc to the posterior indices
        posterior.V(:,end+1) = zeros(size(posterior.V,1),1); % to add a tc value which was not fitted but always tunable
        
        circuitnamesInSubspace = OutV.circuitnamesInSubspace;
        ndimcircuit = length(circuitnamesInSubspace);
        PosteriorNames = paramSpecsNames(PIdxPosterior);
        index_in_PosteriorNames = zeros(1,ndimcircuit);
        
        copynumbers_idx = find(cellfun(@isempty,regexp(circuitnamesInSubspace,'k\d\w\w'))==0);
        for iparaCircuitSubspace = 1:ndimcircuit            
            if length(circuitnamesInSubspace{iparaCircuitSubspace}) > 6
                index_in_PosteriorNames(iparaCircuitSubspace) = find(cellfun(@isempty,regexp(PosteriorNames,[circuitnamesInSubspace{iparaCircuitSubspace}(1:3) '\w\w\w']))==0);
            elseif ismember(iparaCircuitSubspace,copynumbers_idx)
                index_in_PosteriorNames(iparaCircuitSubspace) = find(cellfun(@isempty,regexp(PosteriorNames,[circuitnamesInSubspace{iparaCircuitSubspace}(1:3) '\w\w']))==0);
            else
                index_in_PosteriorNames(iparaCircuitSubspace) = find(ismember(PosteriorNames,circuitnamesInSubspace{iparaCircuitSubspace}));
            end                        
        end             


        posterior_circuitSubspace = [];
        posterior_circuitSubspace.V = posterior.V(:,index_in_PosteriorNames);        

        %% update feasibility by tuning only copy numbers and tc

        if  ~isfield(OutV,'updated_feasibility') 

            fprintf('RankProjections, update feasibility #%d/%.2f \n',iMod,nMod);            
            find_index_function = @find_tuning_idx_copynumbers_tc;   

            %  optimize feasibility by tuning all parameters            
            [OutV.updated_feasibility, OutV.updated_tuning_factors,  OutV.updated_idx_viable] = tune_feasibility(OutV, posterior_circuitSubspace, find_index_function, paramSpecs, mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);                     
            iSaveOutV([newfoldername sprintf('OutV_%d',iMod)],OutV); %save OutV again

        end   
        
        %% compute feasibility region for our favorite circuit
        
        favorite_circuit_id = 78; % We run this long computation only for iMod = 78 (circuit 39 in the publication)
        
        if isfield(OutV,'updated_feasibility')  && OutV.updated_feasibility > 0  ...
                && (OutV.id == favorite_circuit_id) && ~isfield(OutV,'updated_feasibility_region')                
            
            fprintf('RankProjections, tuning factors region for high feasibility #%d/%.2f \n',iMod,nMod);            
            find_index_function = @find_tuning_idx_copynumbers_tc;   
            tuning_factors_opt = OutV.updated_tuning_factors;

            %  find region of high feasibility by exploring only copy numbers and atc           
            OutV.updated_feasibility_region = tuning_factors_region(tuning_factors_opt, OutV, posterior_circuitSubspace, find_index_function, paramSpecs, mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);                     
            iSaveOutV([newfoldername sprintf('OutV_%d',iMod)],OutV); %save OutV again

        end
        
        % for the k12 copy numbers 0 is part of the feasibility region.
        % we can thus avoid tuning these parameters.
        % update feasibility and compute feasibility region again without tuning these copy numbers.
        
        
        if  ~isfield(OutV,'updated_feasibility_wo_k12') && (OutV.id == favorite_circuit_id)

            fprintf('RankProjections, update feasibility without tuning k12 promoters #%d/%.2f \n',iMod,nMod);            
            find_index_function = @find_tuning_idx_k2a_tc;   

            %  optimize feasibility by tuning all parameters            
            [OutV.updated_feasibility_wo_k12, OutV.updated_tuning_factors_wo_k12,  OutV.updated_idx_viable_wo_k12] = tune_feasibility(OutV, posterior_circuitSubspace, find_index_function, paramSpecs, mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);                     
            iSaveOutV([newfoldername sprintf('OutV_%d',iMod)],OutV); %save OutV again

        end  
        
        if isfield(OutV,'updated_feasibility_wo_k12')  && OutV.updated_feasibility_wo_k12 > 0  ...
                && (OutV.id == favorite_circuit_id) && ~isfield(OutV,'updated_feasibility_region_wo_k12')                        
            
            fprintf('RankProjections, tuning factors region for high feasibility without tuning k12 promoters #%d/%.2f \n',iMod,nMod);            
            find_index_function = @find_tuning_idx_k2a_tc;   
            tuning_factors_opt = OutV.updated_tuning_factors_wo_k12;

            %  find region of high feasibility by exploring only copy numbers and atc           
            OutV.updated_feasibility_region_wo_k12 = tuning_factors_region(tuning_factors_opt, OutV, posterior_circuitSubspace, find_index_function, paramSpecs, mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);                     
            iSaveOutV([newfoldername sprintf('OutV_%d',iMod)],OutV); %save OutV again

        end
                
           
    end

end

       
