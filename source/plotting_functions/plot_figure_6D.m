function [] = plot_figure_6D()

loaded = load('output_files/data/dataPulsealpha_aTc.mat');
dataPulsealpha = loaded.dataPulsealpha;

strains = {'FRY2952' 'FRY2958'};

% compute or load already computed prediction 
 if isfile('output_files/prediction/predicted_data.mat')
    loaded = load('output_files/prediction/predicted_data.mat');
    Citrine_6h_aTcopt = loaded.Citrine_6h_aTcopt;
else
    Citrine_6h_aTcopt = simulate_populations_T39;
end
% rescale concentration to observed fluorescence given data
rho       = 0.0077;
k_maturation = 0.0173;
datacontrol0 = mean(10.^dataPulsealpha.LogNormalizedCitrine_gated.FRY2932{1});
datacontrolAct1 = mean(10.^dataPulsealpha.LogNormalizedCitrine_gated.FRY2908{1});
x_scal = rho*(k_maturation + rho)/k_maturation*(datacontrolAct1 - datacontrol0); 
FPlognoise = normrnd(0,std(dataPulsealpha.LogNormalizedCitrine_gated.FRY2932{1}),1000,3);
FP = log10(Citrine_6h_aTcopt.*x_scal + datacontrol0) + FPlognoise;

figure()
npulses = length(dataPulsealpha.alphapulse);
map = lines(npulses);

nstrains = numel(strains);
for kstrain = 1:nstrains
    strain = strains{kstrain};
    data = dataPulsealpha.LogNormalizedCitrine_gated.(strain);
    if kstrain == 1
        kdose = 1;
    else 
        kdose = 3;
    end

    subplot(1,3,kstrain)

    for kalphapulse = 1:npulses
        pd = fitdist(data{kdose,kalphapulse},'Kernel');
        X = -3:0.1:1;
        Y = pdf(pd,X);
        plot(X,Y,'Color',map(kalphapulse,:),'LineWidth',2);
        set(gca,'FontSize',20);
        hold on;
    end
    
    xlim([-3 1])  
    ylim([0 3.5])      
    xlabel('Normalized fluorescence')   
    

end


subplot(1,3,3)

for kalphapulse = 1:npulses
    pd = fitdist(FP(:,kalphapulse),'Kernel','Kernel','normal');
    X = -3:0.1:1;
    Y = pdf(pd,X);
    plot(X,Y,'Color',map(kalphapulse,:),'LineWidth',2);
    set(gca,'FontSize',20);
    hold on;
end        


subplot(1,3,1);
title('C_3');
ylabel('pdf')
ylim([0 3.5])
subplot(1,3,2);
yticks([]);
ylim([0 3.5]);
title('T_{39}');
subplot(1,3,3);
yticks([]);
ylim([0 3.5]);
xlim([-3 1])
title('T_{39} model prediction');

legendCell = strcat(string(num2cell(dataPulsealpha.alphapulse)),'min');
legend(legendCell);
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 0.5]);
print('plots/figure_6D', '-dpdf'); 


end