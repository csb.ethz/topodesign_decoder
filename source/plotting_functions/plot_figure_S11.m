% © 2021, ETH Zurich
function [] = plot_figure_S11()

    loaded = load('output_files/data/dataDRaTc.mat');
    dataDRaTc = loaded.dataDRaTc;
    ktime = 2;
    strainnames = { 'FRY2931' 'FRY2908' 'FRY2932'};
    yCLnames = {'yCL110-PlexAtet-Citrine' 'yCL106-Pact1-Citrine' 'yCL102-empty'};

    figure()
    clf;
    
    normalized = [0 1];
    aTcdose = [0 250];
    for knorm = 1:2
        subplot(2,1,knorm)
        legendCell = [];
        for kdose = 1:2                                            
            for kalphadose = 1:2
                legendCell = plot_hist_strains_aTc(dataDRaTc,strainnames,yCLnames,aTcdose(kdose),kalphadose, ktime,normalized(knorm),legendCell);        
                hold on;
            end
            title(sprintf('Response at %dh, Normalized' ,dataDRaTc.timepoints(ktime)));            
        end
        if knorm ==1
            legend(legendCell);
        end
    end
    
    set(gcf,'PaperOrientation','portrait','PaperUnits','Normalized','PaperPosition',[0 0 1 0.7]);
    print('plots/figure_S11', '-dpdf'); 

end