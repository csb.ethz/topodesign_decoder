% © 2021, ETH Zurich
function [] = plot_figure_4D()

%% load posterior
loaded = load('output_files\posterior\posterior191003.mat');
paramSpecs = readtable('source/models/paramSpecs.txt');
paramSpecsNames = paramSpecs.names;
posterior = loaded.OutV;
tc_idx = find(ismember(paramSpecs.names,'tc'));
PIdxPosterior = [loaded.PIdx; tc_idx]; %adding tc to the posterior indexes
PosteriorNames = paramSpecsNames(PIdxPosterior);
posterior.V(:,end+1) = zeros(size(posterior.V,1),1); % to add a tc value which was not fitted but always tunable

%% load topology 39 
loaded = load('output_files\ranked_topologies\190927_172938\OutV_78.mat');
OutV = loaded.OutV;
circuitnamesInSubspace = OutV.circuitnamesInSubspace;
ndimcircuit = length(circuitnamesInSubspace);
index_in_PosteriorNames = zeros(1,ndimcircuit);

%% map posterior to topology subspace
copynumbers_idx = find(cellfun(@isempty,regexp(circuitnamesInSubspace,'k\d\w\w'))==0);
for iparaCircuitSubspace = 1:ndimcircuit            
    if length(circuitnamesInSubspace{iparaCircuitSubspace}) > 6
        index_in_PosteriorNames(iparaCircuitSubspace) = find(cellfun(@isempty,regexp(PosteriorNames,[circuitnamesInSubspace{iparaCircuitSubspace}(1:3) '\w\w\w']))==0);
    elseif ismember(iparaCircuitSubspace,copynumbers_idx)
        index_in_PosteriorNames(iparaCircuitSubspace) = find(cellfun(@isempty,regexp(PosteriorNames,[circuitnamesInSubspace{iparaCircuitSubspace}(1:3) '\w\w']))==0);
    else
        index_in_PosteriorNames(iparaCircuitSubspace) = find(ismember(PosteriorNames,circuitnamesInSubspace{iparaCircuitSubspace}));
    end                        
end             

posterior_circuitSubspace = [];
posterior_circuitSubspace.V = posterior.V(:,index_in_PosteriorNames);
ndimCircuit = length(OutV.circuitnamesInSubspace);
factors = zeros(1,ndimCircuit);
find_index_function = @find_tuning_idx_k2a_tc; 
[tuning_index_in_circuitSubspace, ~] = find_index_function(circuitnamesInSubspace);
factors(tuning_index_in_circuitSubspace) = OutV.updated_tuning_factors_wo_k12;
moved_fitted_region.V = posterior_circuitSubspace.V + factors;

%% plot parameter posterior on 1d

paramSpecsCircuit = paramSpecs(PIdxPosterior(index_in_PosteriorNames),:);
para1 = 12;
para2 = 3;

figure()

[~,density,X,Y] = kde2d([OutV.V(:,para1),OutV.V(:,para2)],100);
contour(X,Y,density,'LineWidth',1);
hold on;

[~,density,X,Y] = kde2d([moved_fitted_region.V(:,para1),moved_fitted_region.V(:,para2)],100);
contour(X,Y,density,'LineWidth',1);
contourcmap('bone');

hold on;
[~,density,X,Y] = kde2d([posterior_circuitSubspace.V(:,para1),posterior_circuitSubspace.V(:,para2)],100);
contour(X,Y,density,'LineWidth',1);
contourcmap('bone');

ylim([0 10])
xlim([-0.5 2])
xlabel(OutV.circuitnamesInSubspace{para1});
ylabel(OutV.circuitnamesInSubspace{para2});
set(gca,'FontSize',20);
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
set(gca,'TickLength',[0 0]);
print('plots/figure_4D', '-dpdf'); 


end