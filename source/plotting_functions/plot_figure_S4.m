% © 2021, ETH Zurich
function [] = plot_figure_S4()
    loaded = load('output_files\ranked_topologies\190927_172938\RankedCircuits.mat');
    RankedCircuits = loaded.RankedCircuits;
    [~,index] = sortrows([RankedCircuits.logrobustness].'); RankedCircuits = RankedCircuits(index); clear index
    idxtopo = find([RankedCircuits.ideal_feasibility]>0.9);
    ntopo = length(idxtopo);
    IDtopo = zeros(ntopo,1);
    publication_idtopo = zeros(ntopo,1);
    Robustness = zeros(ntopo,1);
    for ktopo = 1:ntopo
        IDtopo(ktopo) = RankedCircuits(idxtopo(ktopo)).OutV_id;
        publication_idtopo(ktopo) = RankedCircuits(idxtopo(ktopo)).publication_id;
        Robustness(ktopo) = RankedCircuits(idxtopo(ktopo)).logrobustness;
    end

    n1 = zeros(1000,ntopo);
    kL1 = zeros(1000,ntopo);
    n2 = zeros(1000,ntopo);
    kL2 = zeros(1000,ntopo);

    Circuitnames = cell(ntopo,1);
    for ktopo = 1:ntopo      
        loaded = load(sprintf('output_files/ranked_topologies/190927_172938/OutV_%d.mat',IDtopo(ktopo)));
        OutV = loaded.OutV;
        idxn1 = find(ismember(OutV.circuitnamesInSubspace,'n1'));
        idxkL1 = find(ismember(OutV.circuitnamesInSubspace,'kL1'));
        idxn2 = find(ismember(OutV.circuitnamesInSubspace,'n2'));
        idxkL2 = find(ismember(OutV.circuitnamesInSubspace,'kL2'));
        n1(:,ktopo) = OutV.V(1:1000,idxn1)';
        kL1(:,ktopo) = OutV.V(1:1000,idxkL1)';
        n2(:,ktopo) = OutV.V(1:1000,idxn2)';
        kL2(:,ktopo) = OutV.V(1:1000,idxkL2)';
        Circuitnames{ktopo} = OutV.circuitnames';
    end

    %% figure S3
    map = flipud(copper(ntopo));
    figure(1);
    clf;
    for ktopo = 1:ntopo
        subplot(2,2,1)
        pd = fitdist(n1(:,ktopo),'Kernel');
        X = 0:0.1:10;
        Y = pdf(pd,X);
        plot(X,Y,'Color',map(ktopo,:),'LineWidth',2);
        xlabel('n_1')
        ylabel('pdf')
        xlim([0 10])
        set(gca,'FontSize',16);
        hold on;

        subplot(2,2,2)
        pd = fitdist(kL1(:,ktopo),'Kernel');
        X = -4:0.1:log10(0.5);
        Y = pdf(pd,X);
        plot(X,Y,'Color',map(ktopo,:),'LineWidth',2);
        hold on;
        xlabel('Log_{10}(k_{L1})')
        xlim([-4 log10(0.5)])
        set(gca,'FontSize',16);

        subplot(2,2,3)
       pd = fitdist(n2(:,ktopo),'Kernel');
        X = 0:0.1:10;
        Y = pdf(pd,X);
        plot(X,Y,'Color',map(ktopo,:),'LineWidth',2);
        xlabel('n_2')
        ylabel('pdf')
        xlim([0 10])
        set(gca,'FontSize',16);
        hold on;

        subplot(2,2,4)
        pd = fitdist(kL2(:,ktopo),'Kernel');
        X = -4:0.1:log10(0.5);
        Y = pdf(pd,X);
        plot(X,Y,'Color',map(ktopo,:),'LineWidth',2);
        hold on;
        xlabel('Log_{10}(k_{L2})')
        xlim([-4 log10(0.5)])
        set(gca,'FontSize',16);
    end

    set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
    print('plots/figure_S4', '-dpdf'); 
end