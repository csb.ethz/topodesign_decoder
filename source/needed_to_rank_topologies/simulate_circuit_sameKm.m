% © 2021, ETH Zurich
function [ cost, paramValues,  SubspaceIdx] = simulate_circuit_sameKm(varargin)
% initialize with [ cost, paramValues,  SubspaceIdx ] = simulate_circuit_sameKm(mexNames, expData, paramNames, islog, fixedParamValuesTail, circuitnames); 
% simulates a parameter set given in the same Km subspace (directly from
% OutV)
% returns the indices of the same Km sampling subspace in the referential of circuit
% names

persistent  KmIdx KmIdxVector circuitnames

if nargin > 1
    
    mexNames = varargin{1};
    expData = varargin{2};
    paramNames = varargin{3};
    islog = varargin{4};
    fixedParamValuesTail = varargin{5};
    simopts = varargin{6};
    circuitnames = varargin{7};
    [ cost, paramValues ] = evalModelDirect(mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts);     
    Km1idx = find(~contains(circuitnames,'Km1')==0);
    Km2idx = find(~contains(circuitnames,'Km2')==0);
    Kmaidx = find(cellfun(@isempty,regexp(circuitnames,'Kma\d'))==0);
    KmIdx = {Km1idx; Km2idx; Kmaidx};   
    KmIdxVector = [Km1idx(2:end);Km2idx(2:end);Kmaidx(2:end)]; 
    SubspaceIdx = setdiff(1:length(circuitnames)',KmIdxVector);
    
else
    
    SubspaceIdx = setdiff(1:length(circuitnames)',KmIdxVector);

    ParamValuesInSubspace = varargin{1};
    
    variableParamValuesHead = zeros(length(circuitnames),1);
    
    variableParamValuesHead(SubspaceIdx) = ParamValuesInSubspace;

    for iKm = 1:3
        u = KmIdx{iKm};
        if ~isempty(u)
            for k = 2:length(u)
                ref = u(1);
                idx = u(k);
                variableParamValuesHead(idx) = ParamValuesInSubspace(ref);
            end
        end
    end

    [ cost, paramValues ] = evalModelDirect(variableParamValuesHead);

    
    
end