% © 2021, ETH Zurich
function minstd = find_minstd(dataset)
% regress the std vs measurement values to find the limit of detection,
% which sets up a minimum standard deviation for all data points.
flatmeans = [];
flatstd = [];
for idata = 1:numel(dataset)

    fields = fieldnames(dataset{idata});
    idx_array = contains(fieldnames(dataset{idata}),'array'); 
    if any(idx_array)
        cond_array = fields{idx_array};  % cond_array can be aTc_array or alpha_array depending on the conditions of the experiment
        ncond = numel(dataset{idata}.(cond_array));
    else
        ncond = 1;
    end

    for kcond = 1:ncond

        if any(idx_array)
            data = dataset{idata}.(cond_array){kcond};    % aTc exp done with 2 alpha conditions                                                        
            meancontrol0 = dataset{idata}.(cond_array){1}.FRY2932.means(1); % any time point works since this strain is not impacted
            stdcontrol0 = dataset{idata}.(cond_array){1}.FRY2932.std(1);  
        else
            data = dataset{idata};
            meancontrol0 = data.FRY2932.means(1,1);
        end
    end

    for kcond = 1:ncond
        idx_strains = logical(contains( fieldnames(data),'FRY') + contains( fieldnames(data),'yCL'));
        fields = fieldnames(data);
        strainnames = fields(idx_strains);
        for kstrain = 1:numel(strainnames)                                			                
            datameans = data.(strainnames{kstrain}).means - meancontrol0;
            flatmeans = [flatmeans, reshape(datameans,[1,numel(datameans)])];
            datastd = sqrt(data.(strainnames{kstrain}).std.^2 + stdcontrol0^2 );
            flatstd = [flatstd, reshape(datastd,[1,numel(datastd)])];
        end
    end
end

X = [ones(size(flatmeans')) flatmeans'];
b = regress(flatstd', X);
minstd = b(1);

end