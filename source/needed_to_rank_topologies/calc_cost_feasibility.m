% © 2021, ETH Zurich
function [costfeasibility,  idx_viable] = calc_cost_feasibility(varargin)
% calculate the feasibility by counting how many fitted points are viable

% initialize first with  calc_cost_feasibility(tuning_factors_start, tuning_index_in_circuitSubspace, ...
%         OutV, posterior,plotFlag,allSamplesFlag, ...
%         mexNames, expData, paramNames, islog, fixedParamValuesTail, circuitnames);


    persistent tuning_index_in_circuitSubspace OutV posterior allSamplesFlag

        tuning_factors = varargin{1};


    if nargin > 1 % initialize the function

        tuning_index_in_circuitSubspace = varargin{2};
        OutV = varargin{3};
        posterior = varargin{4};
        allSamplesFlag = varargin{5};    
        mexNames = varargin{6};
        expData = varargin{7};
        paramNames = varargin{8};
        islog = varargin{9};
        fixedParamValuesTail = varargin{10};
        simopts = varargin{11};
        circuitnames = varargin{12};
        simulate_circuit_sameKm(mexNames, expData, paramNames, islog, fixedParamValuesTail, simopts, circuitnames);  % initialize
        costfeasibility = 0;
        idx_viable = [];

    else

    %% move the fitted region according to the tuning factors

        ndimCircuit = length(OutV.circuitnamesInSubspace);
        factors = zeros(1,ndimCircuit);
        factors(tuning_index_in_circuitSubspace) = tuning_factors;
        moved_posterior = [];
        moved_posterior.V = posterior.V + factors;
        npointsfit = size(moved_posterior.V,1); 


        if ~ allSamplesFlag
            nSamples = min(floor(npointsfit/10),1000);            
        else
            nSamples = npointsfit;
        end
        
        ntimesinside = 0;
        idx_viable_samples = zeros(nSamples,1);
        RandomPerm = randperm(npointsfit);
        randFitPoints = RandomPerm(1:nSamples);


        for iSample = 1:nSamples            
            ipointfit = randFitPoints(iSample);
            paramInSubspace = moved_posterior.V(ipointfit,:);  
            viableCostPointsFit = simulate_circuit_sameKm(paramInSubspace);
            idx_viable_samples(iSample) = (viableCostPointsFit < 1);
            ntimesinside = ntimesinside + (viableCostPointsFit < 1);
        end
        idx_viable = randFitPoints(find(idx_viable_samples));
        costfeasibility = 1 - ntimesinside/nSamples;

    end
              
end