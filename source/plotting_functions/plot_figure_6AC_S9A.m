% © 2021, ETH Zurich
function [] = plot_figure_6AC_S9A


    foldername = 'plots\figure_6A_S9A\';
    if ~exist(foldername)
        mkdir(foldername);
    end

    loaded = load('output_files/data/dataPulsealpha_aTc.mat');
    dataPulsealpha = loaded.dataPulsealpha;
    strains = dataPulsealpha.strains;
    nstrains = numel(strains);
    dataPulsealpha.aTcdose(12) = 5; % to be able to plot in log. Will be put back to 0 in illustrator
    [percent_higher, threshold ] = calc_percent_higher(dataPulsealpha);
    fprintf(['threshold = %d \n'], threshold)    

    
    % load prediction
     loaded = load('output_files/prediction/predicted_data.mat');
     Citrine_6h_aTc = loaded.Citrine_6h_aTc;
     Citrine_6h_opt = loaded.Citrine_6h_aTcopt;
    % rescale concentration to observed fluorescence given data
    rho       = 0.0077;
    k_maturation = 0.0173;
    datacontrol0 = mean(10.^dataPulsealpha.LogNormalizedCitrine_gated.FRY2932{1});
    datacontrolAct1 = mean(10.^dataPulsealpha.LogNormalizedCitrine_gated.FRY2908{1});
    x_scal = rho*(k_maturation + rho)/k_maturation*(datacontrolAct1 - datacontrol0); 
%     FP = Citrine_6h_aTc.*x_scal + datacontrol0;
    FPlognoise = normrnd(0,std(dataPulsealpha.LogNormalizedCitrine_gated.FRY2932{1}),300,12,3);
    FPlognoise_opt = normrnd(0,std(dataPulsealpha.LogNormalizedCitrine_gated.FRY2932{1}),1000,1,3);
% FP = log10(Citrine_6h_aTcopt.*x_scal) + FPlognoise;
    FP = log10(Citrine_6h_aTc.*x_scal + datacontrol0) + FPlognoise;
    FPopt = log10(Citrine_6h_opt.*x_scal + datacontrol0) + FPlognoise_opt;
    durations = [0 30 180];
    percent_higher_pred = zeros(12,3);
    percent_higher_pred_opt = zeros(1,3);

    ncells = size(FP,1);
    for kduration = 1:3
        for katc = 1:12
            percent_higher_pred(katc,kduration) = 100*numel(find(FP(:,katc,kduration)>threshold))/ncells;             
        end
        percent_higher_pred_opt(kduration) = 100*numel(find(FPopt(:,kduration)>threshold))/size(FPopt,1); 
    end

    for kstrain = 1:nstrains

        figure()
        cmap = parula(10);
        % percent_higher(kdose,kstrain,kalphapulse)
        plotted_markers = plot(log10(dataPulsealpha.aTcdose),squeeze(percent_higher(:,kstrain,:)),'o'); hold on;   
        plotted_lines = plot(log10(dataPulsealpha.aTcdose),squeeze(percent_higher(:,kstrain,:)),'-'); hold on;
        if kstrain == 2
            plotted_prediction = plot(log10(dataPulsealpha.aTcdose),percent_higher_pred,'--'); 
        end
        xticks(flip(log10(dataPulsealpha.aTcdose)));
        set(gca,'xticklabel',[0 flip(dataPulsealpha.aTcdose(1:(end-1)))])
        xtickangle(90);
        xlim([log10(5) log10(400)])
        ylabel('cells ON at 6h (%)');    
        legend('0 min pulse', '30min pulse', '3h pulse')

        set(plotted_markers(1),'color',cmap(9,:),'MarkerSize',8)
        set(plotted_lines(1),'color',cmap(9,:))
        
        set(plotted_markers(2),'color',cmap(6,:),'MarkerSize',8);
        set(plotted_lines(2),'color',cmap(6,:));
        
        set(plotted_markers(3),'color',cmap(2,:),'MarkerSize',8);
        set(plotted_lines(3),'color',cmap(2,:));
        
        if kstrain == 2
            set(plotted_prediction(1),'color',cmap(9,:))
            set(plotted_prediction(2),'color',cmap(6,:));
            set(plotted_prediction(3),'color',cmap(2,:));
        end
        hold on;


        title(sprintf('T_{39.%d}',kstrain))
        set(gca,'TickLength',[0 0],'LineWidth',0.5,'FontSize',9);
        ylim([0 100])
        set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 0.8 1]);

        print(sprintf('plots/figure_6A_S9A/aTc_opt_variant_%d',kstrain), '-dpdf'); 
    end
    
    
    
    %% bar plot
    
    npulses = 3;
    barplotdata = zeros(nstrains, npulses);
    aTc_indices = [4 3 7 6 9 11 11];
    for kstrain = 1:7
        barplotdata(kstrain,:) = percent_higher(aTc_indices(kstrain),kstrain,:);
        fprintf('fold strain %d = %d \n',kstrain,percent_higher(aTc_indices(kstrain),kstrain,2)/percent_higher(aTc_indices(kstrain),kstrain,3))
    end
    % for prediction best katc = 2
    barplotdata(8,:) = percent_higher_pred_opt;
    fprintf('fold simulated strain = %d \n',percent_higher_pred_opt(2)/percent_higher_pred_opt(3))
    
    figure();
    bar(barplotdata)
    ylim([0 100])
    set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 0.8 1]);
    print('plots/figure_6C', '-dpdf'); 


end