% © 2021, ETH Zurich
%% Decoder model via coupled FFLs, analogous to Mangan & Alon, PNAS 2002

function [dx] = ode_decoder(t,x,varargin)

persistent ux By bey Kxy dy Bz bez Kxz Kyz dz H Bo beo Kzo Kyo do ...
    KyzNF KzyNF dyNF dzNF idxstate

dx = [];

if isempty(t) % initialization
    
    para = varargin{1}; % parameters
    By    = para(1); 
    bey   = para(2); 
    Kxy   = para(3); 
    dy    = para(4); 
    Bz    = para(5); 
    bez   = para(6); 
    Kxz   = para(7); 
    Kyz   = para(8); 
    dz    = para(9); 
    H     = para(10); 
    Bo    = para(11); 
    beo   = para(12); 
    Kzo   = para(13); 
    Kyo   = para(14); 
    do    = para(15); 
    KyzNF = para(16);
    KzyNF = para(17);
    dyNF  = para(18);
    dzNF  = para(19);
    
    ux  = varargin{2}; % input
    idxstate = varargin{3}; % state index    
    
else % computing r.h.s.
    
    [dx] = [funiff(x(idxstate{1}),ux,By,bey,Kxy,dy,Bz,bez,Kxz,Kyz,dz,H); ...
        funcff(x(idxstate{2}),ux,Bz,bez,Kyz,dz,Bo,beo,Kzo,Kyo,do,H); ...
        funecff(x(idxstate{3}),ux,By,bey,Kxy,dy,Bz,bez,Kyz,dz,Bo,beo,Kzo,Kyo,do,H); ...
        fundec(x(idxstate{4}),ux,By,bey,Kxy,dy,Bz,bez,Kxz,Kyz,dz,Bo,beo,Kzo,Kyo,do,H); ...
        funnf(x(idxstate{5}),ux,By,bey,KzyNF,dyNF,Bz,bez,Kxz,KyzNF,dzNF,H); ...
        fundecnf(x(idxstate{6}),ux,By,bey,KzyNF,dyNF,Bz,bez,Kxz,KyzNF,dzNF,Bo,beo,Kzo,Kyo,do,H); ...
        x(end-1)];
    
end

return

%% Auxiliary functions

% function IFF
function [dx] = funiff(x,ux,By,bey,Kxy,dy,Bz,bez,Kxz,Kyz,dz,H)

dx = [By + bey*funact(ux,Kxy,H) - dy*x(1); ...
      Bz + bez*funprom(ux,x(1),Kxz,Kyz,H) - dz*x(2)];
return

% function CFF
function [dx] = funcff(x,ux,Bz,bez,Kyz,dz,Bo,beo,Kzo,Kyo,do,H)

dx = [Bz + bez*funrep(ux,Kyz,H) - dz*x(1); ...
      Bo + beo*funprom(x(1),ux,Kzo,Kyo,H) - do*x(2)];
return

% function extended CFF
function [dx] = funecff(x,ux,By,bey,Kxy,dy,Bz,bez,Kyz,dz, ...
    Bo,beo,Kzo,Kyo,do,H)

dx = [By + bey*funact(ux,Kxy,H) - dy*x(1); ...
      Bz + bez*funrep(x(1),Kyz,H) - dz*x(2); ...
      Bo + beo*funprom(x(2),x(1),Kzo,Kyo,H) - do*x(3)];
return

% function decoder, IFF-based
function [dx] = fundec(x,ux,By,bey,Kxy,dy,Bz,bez,Kxz,Kyz,dz, ...
    Bo,beo,Kzo,Kyo,do,H)

dx = [funiff(x,ux,By,bey,Kxy,dy,Bz,bez,Kxz,Kyz,dz,H); ... % Y,Z
      Bo + beo*funprom(x(2),x(1),Kzo,Kyo,H) - do*x(3)]; % O
return

% function NF
function [dx] = funnf(x,ux,By,bey,Kzy,dy,Bz,bez,Kxz,Kyz,dz,H)

dx = [By + bey*funact(x(2),Kzy,H) - dy*x(1); ...
      Bz + bez*funact(ux,Kxz,H)*funrep(x(1),Kyz,H) - dz*x(2)];
return

% function decoder, NF-based
function [dx] = fundecnf(x,ux,By,bey,Kzy,dy,Bz,bez,Kxz,Kyz,dz, ...
    Bo,beo,Kzo,Kyo,do,H)

dx = [funnf(x,ux,By,bey,Kzy,dy,Bz,bez,Kxz,Kyz,dz,H); ... % Y,Z
      Bo + beo*funprom(x(2),ux,Kzo,Kyo,H) - do*x(3)]; % O
return

% regulation function activator
function [f] = funact(u,K,H)
f = (u/K)^H/(1+(u/K)^H);
return

% regulation function repressor
function [f] = funrep(u,K,H)
f = 1/(1+(u/K)^H);
return

% regulation function promoter (AND, activated by X, repressed by Y)
function [G] = funprom(ux,uy,Kxz,Kyz,H)
G = funact(ux,Kxz,H)*funrep(uy,Kyz,H);
return