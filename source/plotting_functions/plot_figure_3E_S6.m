% © 2021, ETH Zurich
function [] = plot_figure_3E_S6()

loaded = load('output_files\posterior\posterior191003.mat');
OutV = loaded.OutV;
PIdx = loaded.PIdx;
paramSpecs = readtable('source/models/paramSpecs.txt');
islog = find(paramSpecs.islog(PIdx));
bounds = [paramSpecs.bmin(PIdx) paramSpecs.bmax(PIdx)];
names = paramSpecs.names(PIdx);

logbounds = bounds;
logbounds(islog,:) = log10(bounds(islog,:));
% 1 = d1 9=k12 5=n1 12 = k2a

figure()
clf;
subplot(1,2,1)
para1 = 9;
para2 = 1;

[~,density,X,Y] = kde2d([OutV.V(:,para1),OutV.V(:,para2)],50);
contour(X,Y,density,'LineWidth',1);
contourcmap('bone');
xlabel(names{para1});
ylabel(names{para2});
% xlim(logbounds(para1,:));
xlim([-1 2]);
ylim(logbounds(para2,:));
% ylim([5 10])
set(gca,'TickLength',[0 0 ]);

subplot(1,2,2)
para2 = 5;
para1 = 12;
[~,density,X,Y] = kde2d([OutV.V(:,para1),OutV.V(:,para2)],50);
contour(X,Y,density,'LineWidth',1);
contourcmap('bone');
xlabel(names{para1});
ylabel(names{para2});
% xlim(logbounds(para1,:));
xlim([-1 2]);
ylim(logbounds(para2,:));
% ylim([5 10])
set(gca,'TickLength',[0 0 ]);
set(findall(gcf,'-property','FontSize'),'FontSize',14)
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 0.5]);
print('plots/figure_3E', '-dpdf');   

%% figure S5
plot_OutV(OutV,paramSpecs(PIdx,:))
%%
set(gcf,'PaperOrientation','portrait','PaperUnits','Normalized','PaperPosition',[0 0 1 0.7]);
print('plots/figure_S6', '-dpdf');   


end