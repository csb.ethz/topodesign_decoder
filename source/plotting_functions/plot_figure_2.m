% © 2021, ETH Zurich

function [] = plot_figure_2()


    foldername = 'plots/figure_2'; 
    if ~exist(foldername)
        mkdir(foldername);
    end

    loaded = load('output_files/ranked_topologies/190927_172938/RankedCircuits.mat');
    RankedCircuits = loaded.RankedCircuits;

    circuit_ids = [16 30 39 74];

    for kcircuit = 1:length(circuit_ids)
            circuit_id = circuit_ids(kcircuit);
            idx = find([RankedCircuits.publication_id] == circuit_id);
            fprintf('paper_id = %d \n',circuit_id);
            disp(RankedCircuits(idx).circuitnames);
            OutV_id = RankedCircuits(idx).OutV_id;
            loaded = load(sprintf('output_files/ranked_topologies/190927_172938/OutV_%d.mat',OutV_id));
            OutV = loaded.OutV;
            figure();
            idx_sample = randi(length(OutV.V));
            sim_OutV(OutV,idx_sample);
            subplot(4,1,1)
            set(gcf,'PaperOrientation','portrait','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
            print(sprintf('plots/figure_2/circuit_%d_simulation',circuit_id), '-dpdf'); 

    end
end