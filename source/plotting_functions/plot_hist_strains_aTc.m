% © 2021, ETH Zurich
function legendCell = plot_hist_strains_aTc(dataDRaTc,strainnames,yCLnames,aTcdose,kalphadose, ktime,normalized,legendCell)
    
    nstrains = numel(strainnames);
    map = lines(20);
    
    controlstrains = {'FRY2908' 'FRY2932'};
    
    linestyle = {'-' '-.'};
    for kstrain = 1:nstrains
        strainname = strainnames{kstrain};        
        if ismember(strainname,controlstrains) && aTcdose ~= 0
            disp('controls have one dose: aTc = 0')
        else
            
            if  ismember(strainname,controlstrains) && aTcdose == 0
            katcdose = 1; % controls have one dose: aTc = 0            
            else
                katcdose = find(dataDRaTc.aTcdose == aTcdose);
            end
        
            if normalized
               X = -2.5:0.02:1;
               data = dataDRaTc.alpha_array{kalphadose}.(strainname).LogNormalizedCitrine_gated{ktime,katcdose};
            else
               X = 1:0.02:5.5;
               data = dataDRaTc.alpha_array{kalphadose}.(strainname).LogCitrine_gated{ktime,katcdose};
            end      

            pd = fitdist(data,'Kernel');        
            Y = pdf(pd,X);

            plot(X,Y,'Color',map(kstrain + katcdose,:),'LineWidth',2,'LineStyle',linestyle{kalphadose});     
            legendCell{end+1} = sprintf([yCLnames{kstrain} ' %d nM aTc %d nM alpha'],aTcdose,dataDRaTc.alphadose(kalphadose));
            hold on;
            
            if normalized
                xlim([-2.5 1])
            else
                xlim([1 5.5])
            end
        end
    end

    
    
    


end