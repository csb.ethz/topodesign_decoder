#!/bin/bash
#$ -V
#$ -q sc03.q
#$ -N RankTopologiesParallel190927
#$ -cwd


for i in $(seq $SGE_TASK_STEPSIZE);  do
 index=$(($SGE_TASK_ID+$i-1))
 echo "Submitted $index"
 matlab -nosplash  -r "RankTopologies($(($SGE_TASK_ID+$i-1)),'190927_172938'); exit"
done