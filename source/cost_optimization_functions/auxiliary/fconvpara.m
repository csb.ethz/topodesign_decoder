% © 2021, ETH Zurich
function [varargout] = fconvpara(varargin)

% Function: convert parameter structure <-> vector

if nargin == 1, % structure to vector
    arg = varargin{1};
    
    if isstruct(arg),
        s  = fieldnames(arg);
        l  = length(s);
        vector  = zeros(l,1);
        for z=1:l,
            vector(z) = arg.(s{z});
        end
    else
        vector = arg;
        l = length(vector);
    end
    varargout{1} = vector;
    
else % vector to structure
    arg1 = varargin{1}; % vector
    arg2 = varargin{2}; % parameter names (cell)
    l    = length(arg1);
    
    if l ~= length(arg2),
        error('Incompatible sizes of vector / cell array!');
    else
        if ~iscell(arg2), % convert to cell array
            arg2 = cellstr(arg2);
        end
        pstruct = [];
        for z = 1:l,
            pstruct.(arg2{z}) = arg1(z);
        end
        varargout{1} = pstruct;
    end
end
varargout{2} = l;

return
