% © 2021, ETH Zurich
function [topofilter_run, nTopologies] = find_last_finished_topofilter_run()

    output_folder = 'output_files/TopoFilter_output_samples/';
    filestruc = dir([output_folder '*']);
    foldername = filestruc(end).name;
    while ~exist([output_folder foldername '/' foldername '.mat'])
        rmdir([output_folder foldername ],'s');
        disp('The last TopoFilter run was not finished. Deleting the last results folder from output_files/TopoFilter_output_samples/ to rank the topologies of the previous run.');
        filestruc = dir([output_folder '*']);
        foldername = filestruc(end).name;
    end

    topofilter_run = foldername;
    loaded = load([output_folder topofilter_run '/' topofilter_run '.mat']);
    nTopologies = size(loaded.runs{1}.viableProjectionsCollection,1);

end

