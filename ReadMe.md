Info
=====
Source for the TopoDesign method applied to the decoder of short signals presented in [1].
 © 2021, ETH Zurich
 
License
-------
Code and Data are provided under the [MIT License](LICENSE).

Author
-------
Claude Lormeau \
<claude.lormeau@bsse.ethz.ch>

Installation
============
Typical install time (excluding Matlab) on a normal desktop computer: 5 to 10 min

1. Install MATLAB (tested on R2019b).
2. Download the topodesign_decoder package and make sure that only source folders with sub-folders from both HYPERSPACE and TopoFilter toolboxes are on your MATLAB path (w/o examples and tests folders).
3. Download and install the [IQM Tools toolbox v1.2.1](https://iqmtools.intiquan.com). It requires a correctly setup MEX-compiler (mex --setup in MATLAB Command Window).
To reproduce the figures, steps 1 to 3 are sufficient (Mapping toolbox is required in Matlab R2019b).
To reproduce the study, you need additional packages:
4. If you want to take advantage of the parallelisation support, you need to setup the [Parallel Computing Toolbox](https://uk.mathworks.com/products/parallel-computing) .
5. Download [HYPERSPACE toolbox v1.2.1](https://git.bsse.ethz.ch/csb/HYPERSPACE ) , for the parameter space exploration, and the [TopoFilter package](https://git.bsse.ethz.ch/csb/TopoFilter). Make sure that only source folders with sub-folders from both HYPERSPACE and TopoFilter toolboxes are on your MATLAB path (w/o examples and tests folders).
6. Download [Meigo toolbox](http://gingproc.iim.csic.es/meigo.html ), for the parameter optimization.

Usage
=====

* To reproduce the study or parts of the study, run selected sections of the script main_TopoDesign_script located in the source/scripts folder.
* To reproduce the figures of the publication, run selected sections of the script figure_generator_script located in the source/scripts folder.
Typical run time on a normal desktop computer:
	- to reproduce all figures: 2 minutes. 
	- to reproduce the full study: 48h

References
==========
[1]: Claude Lormeau, Fabian Rudolf, Joerg Stelling, A rationally engineered decoder of transient intracellular signals 

