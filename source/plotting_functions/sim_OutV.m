% © 2021, ETH Zurich
function SData = sim_OutV(OutV,k)

% OutV contains details of the topology and its viable points. 
% here we simulate this circuit with the viable parameter set #k

paramSpecs = readtable('source/models/paramSpecs.txt');

% We need to make a vector of estimated parameters paraest which sets a
% value for each parameter of paramSpecs.
% We take the viable samples which are non projected parameters in the same
% Km subspace.

% Map parameters back to one Km per interaction to create the non projected
% parameter vector variableParamValues

Km1idx = find(~contains(OutV.circuitnames,'Km1')==0);
Km2idx = find(~contains(OutV.circuitnames,'Km2')==0);
Kmaidx = find(cellfun(@isempty,regexp(OutV.circuitnames,'Kma\d'))==0);
KmIdx = {Km1idx; Km2idx; Kmaidx};   
KmIdxVector = [Km1idx(2:end);Km2idx(2:end);Kmaidx(2:end)]; 
SubspaceIdx = setdiff(1:length(OutV.circuitnames)',KmIdxVector);


ParamValuesInSubspace = OutV.V(k,:);

variableParamValues = zeros(length(OutV.circuitnames),1);

variableParamValues(SubspaceIdx) = ParamValuesInSubspace;

for iKm = 1:3
    u = KmIdx{iKm};
    if ~isempty(u)
        for k = 2:length(u)
            ref = u(1);
            idx = u(k);
            variableParamValues(idx) = ParamValuesInSubspace(ref);
        end
    end
end

% Define which parameters are projected  
notProjected = OutV.circuitnames;
PIdx = find(ismember(paramSpecs.names,notProjected));
FlMode = 1;
islog = paramSpecs.islog(PIdx);

% parameters are log. map them back to linear space
variableParamValues(find(islog)) = 10.^variableParamValues(find(islog));

% create the full estimated parameter set that incorporates projected and
% non-projected values
paraest = paramSpecs.projections;
paraest(PIdx) = variableParamValues;

% simulate the full model with this parameter set
[~,~,~,SData,~] = cost_decoder(paraest, paramSpecs, FlMode, PIdx);
end