% © 2021, ETH Zurich
function [] = plot_figure_1D_S2()

% load ranked circuits
loaded = load('output_files/ranked_topologies/190927_172938/RankedCircuits.mat');
RankedCircuits = loaded.RankedCircuits;
update = 'ideal';

plot_robustness_feasibility(RankedCircuits, update)

ylim([0 1.05])

%% figure 1 D
set(gca,'FontSize',18);
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
set(gca,'TickLength',[0 0]);
print('plots/figure_1D','-dpdf')
%% figure S2
xlim([-12 -6]);
ylim([0.92 1]);
set(gca,'FontSize',18);
set(gcf,'PaperOrientation','landscape','PaperUnits','Normalized','PaperPosition',[0 0 1 1]);
set(gca,'TickLength',[0 0]);
print('plots/figure_S2','-dpdf')

return