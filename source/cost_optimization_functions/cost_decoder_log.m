% © 2021, ETH Zurich
function objfc = cost_decoder_log(varargin)

%takes param in log space as an input.
% and a vector islog of same length

persistent islog;

paralog = varargin{1};

if nargin>1
    islog = varargin{2};
end

islogindices = find(islog);
paralin = paralog;
paralin(islogindices) = 10.^paralog(islogindices);
                    
objfc = cost_decoder(paralin);


return