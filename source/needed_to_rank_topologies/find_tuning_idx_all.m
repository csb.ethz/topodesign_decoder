function [tuning_index_in_circuitspace, tuning_setup] = find_tuning_idx_all(circuitnamesInSubspace)
    tuning_index_in_circuitspace = 1:length(circuitnamesInSubspace);
    tuning_setup = 2*ones(1,length(circuitnamesInSubspace));
end